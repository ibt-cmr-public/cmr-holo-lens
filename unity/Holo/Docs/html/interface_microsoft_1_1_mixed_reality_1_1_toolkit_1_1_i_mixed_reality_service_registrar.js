var interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar =
[
    [ "GetService< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#add36c01c3135eb67d67365441fbc9e85", null ],
    [ "GetServices< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#abbda1118dd07ffbdf28d3e342808b05f", null ],
    [ "IsServiceRegistered< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#a3060a2fe92bc3aeeb5427e8bf169510a", null ],
    [ "RegisterService< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#a3f34653011986702d1afaf770f17596e", null ],
    [ "RegisterService< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#afe11cedaaf529a9a8b4e304eafb9cfcb", null ],
    [ "UnregisterService< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#add34a4848e4488c79046690b88f9245b", null ],
    [ "UnregisterService< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service_registrar.html#abf8518425ccb2c14ea5dca3426814b1b", null ]
];