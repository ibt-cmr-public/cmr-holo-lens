var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller =
[
    [ "TestMotionController", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#a6d8c6ba94b361ace89d5ec5d5d494d50", null ],
    [ "Click", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#a700cfb555878a2dae59d602f4099d517", null ],
    [ "Hide", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#a715a9bdda9dacebbd12a05ac4c8db681", null ],
    [ "Move", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#a6db7113636d4b2a3dacd7f58a3ca474c", null ],
    [ "MoveTo", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#a9e6bd2b71b978295dbedc38b59d80193", null ],
    [ "SelectAndThrowAt", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#aec6e9f9a883a96a956536aae85aba349", null ],
    [ "SetRotation", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#adf1b2c13690664efab4e0d31337fb682", null ],
    [ "SetState", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#a4fdcd3dc28de809df0c34e982fc3b96f", null ],
    [ "Show", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html#ace9313b796b949de0dca16735da2a896", null ]
];