var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions =
[
    [ "AsReadOnly< TElement >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#add18d0052cb6fd85bd770e069730ba1b", null ],
    [ "DisposeElements< TElement >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#a13f8d0df2152065c469870f85a466bea", null ],
    [ "DisposeElements< TElement >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#ae60b94d27829266bd698ea215c16baf3", null ],
    [ "ExportDictionaryValuesAsArray< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#aef91c1e5d384bb2cf724bb8fe7673228", null ],
    [ "GetInteractionByType", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#a47b0e271bde2c69fa1cc8f6955a2b191", null ],
    [ "SortedInsert< TElement >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#ad00b1284e22cf532e2f4515702eb7211", null ],
    [ "SupportsInputType", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#ad362082702725bf05c3e40d85ac0b9c7", null ],
    [ "ToReadOnlyCollection< TElement >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html#ab446197588f2dfd2b4c9704050fcb6e6", null ]
];