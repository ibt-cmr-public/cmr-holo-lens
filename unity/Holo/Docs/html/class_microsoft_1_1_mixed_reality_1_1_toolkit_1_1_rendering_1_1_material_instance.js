var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_rendering_1_1_material_instance =
[
    [ "AcquireMaterial", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_rendering_1_1_material_instance.html#a98f81740a64dcc982199e51f34877faa", null ],
    [ "AcquireMaterials", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_rendering_1_1_material_instance.html#ad4d63772e639b3c8f88d1292be3131be", null ],
    [ "ReleaseMaterial", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_rendering_1_1_material_instance.html#a607c4c4fe101513c0c94540ac2df05f3", null ],
    [ "Material", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_rendering_1_1_material_instance.html#a8fd5767e0a24716ec7e055d3d9616182", null ],
    [ "Materials", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_rendering_1_1_material_instance.html#a0fe3749fde95ea8b58b82227b05ee2e6", null ]
];