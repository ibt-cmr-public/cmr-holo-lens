var dir_547869a01baa2935e7b2b1a5673b9213 =
[
    [ "GameLogic", "dir_934c59ba87b9fb85490abde1d9d5b84f.html", "dir_934c59ba87b9fb85490abde1d9d5b84f" ],
    [ "UI", "dir_12155514c87618e129174ecc88ecad18.html", "dir_12155514c87618e129174ecc88ecad18" ],
    [ "DebugWindow.cs", "_debug_window_8cs.html", [
      [ "MRTK.Tutorials.AzureSpatialAnchors.DebugWindow", "class_m_r_t_k_1_1_tutorials_1_1_azure_spatial_anchors_1_1_debug_window.html", null ]
    ] ],
    [ "GenericNetworkTransmitter.cs", "_generic_network_transmitter_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.GenericNetworkTransmitter", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter" ]
    ] ],
    [ "HTKNetworkManager.cs", "_h_t_k_network_manager_8cs.html", [
      [ "HTKNetworkManager", "class_h_t_k_network_manager.html", "class_h_t_k_network_manager" ]
    ] ],
    [ "ModelWithPlateAnchor.cs", "_model_with_plate_anchor_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.ModelWithPlateAnchor", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_model_with_plate_anchor.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_model_with_plate_anchor" ]
    ] ],
    [ "NetworkDiscoveryWithAnchors.cs", "_network_discovery_with_anchors_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.NetworkDiscoveryWithAnchors", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_network_discovery_with_anchors.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_network_discovery_with_anchors" ],
      [ "HoloToolkit.Examples.SharingWithUNET.NetworkDiscoveryWithAnchors.SessionInfo", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_network_discovery_with_anchors_1_1_session_info.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_network_discovery_with_anchors_1_1_session_info" ]
    ] ],
    [ "PlayerController.cs", "_player_controller_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.PlayerController", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller" ]
    ] ],
    [ "SharedAnchorDebugText.cs", "_shared_anchor_debug_text_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.SharedAnchorDebugText", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_shared_anchor_debug_text.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_shared_anchor_debug_text" ]
    ] ],
    [ "SharedCollection.cs", "_shared_collection_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.SharedCollection", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_shared_collection.html", null ]
    ] ],
    [ "Singleinstance.cs", "_singleinstance_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.SingleInstance< T >", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_single_instance.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_single_instance" ]
    ] ],
    [ "Singleton.cs", "_singleton_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.Singleton< T >", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_singleton.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_singleton" ],
      [ "HoloToolkit.Examples.SharingWithUNET.GameObjectExtensions", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_game_object_extensions.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_game_object_extensions" ]
    ] ],
    [ "UNetAnchorManager.cs", "_u_net_anchor_manager_8cs.html", [
      [ "HoloToolkit.Examples.SharingWithUNET.UNetAnchorManager", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_u_net_anchor_manager.html", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_u_net_anchor_manager" ]
    ] ],
    [ "UNetSharedHologram.cs", "_u_net_shared_hologram_8cs.html", [
      [ "UNetSharedHologram", "class_u_net_shared_hologram.html", "class_u_net_shared_hologram" ]
    ] ]
];