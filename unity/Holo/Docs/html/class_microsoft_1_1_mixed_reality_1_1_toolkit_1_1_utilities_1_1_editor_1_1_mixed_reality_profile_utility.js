var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_profile_utility =
[
    [ "GetProfilePopupOptionsByType", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_profile_utility.html#ae4f982017307f62638f23f1e937fe3ee", null ],
    [ "GetProfilesOfType", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_profile_utility.html#a285dcc298cd21e709fba850ca6db581a", null ],
    [ "GetProfileTypesForService", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_profile_utility.html#a8079f3d6a70bcfca6260a1684193e307", null ],
    [ "IsConcreteProfileType", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_profile_utility.html#a07b758780bb4b0eef26f3a2c1810a503", null ],
    [ "IsProfileForService", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_profile_utility.html#a83b6b6b521af5d968b9258e7e1a7bcf7", null ]
];