var class_color_map =
[
    [ "Click", "class_color_map.html#ad853dcd9c54bdb775808fa9a5c9b99ce", null ],
    [ "FocusEnter", "class_color_map.html#a9c111e19a37324b1e7aceea82aaa56d5", null ],
    [ "FocusExit", "class_color_map.html#af1ee445fcb6c59a069d4d8c27de5bddd", null ],
    [ "ColorButtons", "class_color_map.html#a2a56993be928e0d642be625164e28401", null ],
    [ "DataDisplacementMaterial", "class_color_map.html#aef3ed6bf2cff8908ad64b56f7287feb8", null ],
    [ "DataDisplacementTransparentMaterial", "class_color_map.html#a63fed51544e5b008201cc8c79e4fb5f5", null ],
    [ "DataTurbulenceMaterial", "class_color_map.html#ad0dfecc2bb20dc750f7d63e7a73ed1c2", null ],
    [ "DataVisualizationMaterial", "class_color_map.html#ac53fa4c23d3dfb559d55d9da877020fe", null ],
    [ "LayersLoaded", "class_color_map.html#a98676fbfcc4198e2278de259c39b0bd4", null ],
    [ "MapName", "class_color_map.html#aaa30a2abc36ec39aa1af71ed12f0db68", null ]
];