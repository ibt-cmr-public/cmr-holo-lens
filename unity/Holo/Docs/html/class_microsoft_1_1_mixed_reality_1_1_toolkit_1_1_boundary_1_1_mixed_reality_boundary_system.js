var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system =
[
    [ "MixedRealityBoundarySystem", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system.html#a82983ea9b8776cbd4744845e2ab9151d", null ],
    [ "MixedRealityBoundarySystem", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system.html#a68dba160b669c44d7a6ce9821dcf0d34", null ],
    [ "GetBoundaryGeometry", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system.html#a9efb1cff7aac2834e7e8b4f01e61252a", null ],
    [ "SetTrackingSpace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system.html#a7323d74d5508346d37af20c07c5f37a6", null ],
    [ "IsXRDevicePresent", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system.html#a7e98866263aedeb297ff3fb5e901a124", null ],
    [ "Name", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_mixed_reality_boundary_system.html#a159492486808b7832d15938453fe4c27", null ]
];