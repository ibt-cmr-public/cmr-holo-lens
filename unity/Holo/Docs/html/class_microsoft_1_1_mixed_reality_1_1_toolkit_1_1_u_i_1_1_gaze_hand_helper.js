var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper =
[
    [ "AddSource", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a998c489b5de375e3011fcb6137f45452", null ],
    [ "GetActiveHandCount", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a7a7f112b9ea123fa694b101e231a02c7", null ],
    [ "GetAllHandPositions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#ab003b298c6463c4c39d205070c04c99d", null ],
    [ "GetFirstHand", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#ac12040b11b5d7d2db224e8b6ae36c779", null ],
    [ "GetHandPositionsDictionary", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a5bcf5466ed050f2246190aacfb784ca2", null ],
    [ "GetHandsCentroid", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a1e578da8fee71e606bd9a4e633f94886", null ],
    [ "RemoveSource", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a88ae494484c6654655e23234d4ac89a5", null ],
    [ "RemoveSource", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a4ac13b14d190dee5ae7808eec6703f77", null ],
    [ "TryGetHandPosition", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a4346ad0be84beef4a9239e30986ad5b1", null ],
    [ "TryGetHandPosition", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a21aa9bcc82021623df8ea0fc23000c1e", null ],
    [ "TryGetHandsCentroid", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#a34162dbf4ec7cdc72a22b07297f79e73", null ],
    [ "UpdateSource", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_gaze_hand_helper.html#ab173c473634379e7713018c8baabd5b6", null ]
];