var class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller =
[
    [ "CmdTransform", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#ad3a48119e91d1088effaaa781d327b8d", null ],
    [ "OnStartClient", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#a2e35c94ad774c178fe1d4b519175c6c5", null ],
    [ "OnStartLocalPlayer", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#a694fdc7d9812cbdabb94b5f9b61cf3b0", null ],
    [ "SendImmersedPosition", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#ac6a53484c4acbfdfd63c6b4c77095743", null ],
    [ "SendSharedTransform", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#aef2068af1a2318d65583190181bcebf7", null ],
    [ "SetAnchorOwnerIP", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#a3611882edce507e626776d6a72a0ec3a", null ],
    [ "UpdateAnchorName", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#ad613c193cf9bc08f8054162848581fba", null ],
    [ "AllowManualImmersionControl", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#a86db390d5aacaa4c5493eb15b75d2bdf", null ],
    [ "allPlayers", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#a7e4cff2073a2dd75fa4b18dad2e5c71f", null ],
    [ "PlayerIp", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#ae08e5e7fa47af3d741fd03e5273c2340", null ],
    [ "SharesSpatialAnchors", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#ae7686a98d6e11ad5c78015559dba6f42", null ],
    [ "Instance", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_player_controller.html#a5ea997355226a2563ce96e85e2835c38", null ]
];