var dir_8d8aafaa6f1aa7d0d316d9e082f425c8 =
[
    [ "DirectionalIndicator.cs", "_directional_indicator_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Experimental.Utilities.DirectionalIndicator", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_directional_indicator.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_directional_indicator" ]
    ] ],
    [ "StabilizationPlaneModifier.cs", "_stabilization_plane_modifier_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Experimental.Utilities.StabilizationPlaneOverride", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_stabilization_plane_override.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_stabilization_plane_override" ],
      [ "Microsoft.MixedReality.Toolkit.Experimental.Utilities.StabilizationPlaneModifier", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_stabilization_plane_modifier.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_stabilization_plane_modifier" ]
    ] ],
    [ "WorldAnchorManager.cs", "_world_anchor_manager_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Experimental.Utilities.WorldAnchorManager", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_world_anchor_manager.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_utilities_1_1_world_anchor_manager" ]
    ] ]
];