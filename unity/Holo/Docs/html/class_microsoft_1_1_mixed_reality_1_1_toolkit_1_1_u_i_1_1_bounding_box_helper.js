var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper =
[
    [ "GetFaceBottomCentroid", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#aa278021647062ac10e7cf090539e6204", null ],
    [ "GetFaceCentroid", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a8374692b048ce43246f42f1fbb302767", null ],
    [ "GetFaceCorners", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a032dfa1513a9bf82452cdc9a79358175", null ],
    [ "GetFaceEdgeMidpoints", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a5417586b839ba4c17e301a7e3d157210", null ],
    [ "GetFaceIndices", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a502ceb281de408d1b5ea15b620225125", null ],
    [ "GetFaceNormal", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#ad84224b7123e9eaf66e469f15310dac0", null ],
    [ "GetIndexOfForwardFace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a1bc6ce919ab59b27e55ae53904df8f29", null ],
    [ "GetRawBBCorners", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#ab7f55218675e6bacb4d14072c7dbe748", null ],
    [ "GetUntransformedCornersFromObject", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#afe28280fbbbf0cb5ebcbfbc2199d7ed1", null ],
    [ "UpdateNonAABoundingBoxCornerPositions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a9f645063c3869c04e41c2af70d0798b4", null ],
    [ "UpdateNonAABoundsCornerPositions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box_helper.html#a900a888356da3e7b3e13ef12b60ea936", null ]
];