var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions =
[
    [ "GetChildRectTransformAtPoint", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#a38dc16d1eb94e9739795ff7f74ef9146", null ],
    [ "GetLocalCorners", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#a55d14a8c9c82dfc5c8b3d473b2064844", null ],
    [ "GetPlane", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#a179211693bbd9ba8252688475bd44ea4", null ],
    [ "GetScreenCorners", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#af33854f983003008dc56f37665e92575", null ],
    [ "GetScreenRect", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#a56291d20afa5fcca637cc55b004780fb", null ],
    [ "GetViewportCorners", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#a2b3cb1b0db0469750fc2d72a59f78acb", null ],
    [ "GetWorldCorners", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#a38e6dbb8a350cb393497a9ffff786c9b", null ],
    [ "Raycast", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html#ae85d545ae8f76a72ff2242d383456949", null ]
];