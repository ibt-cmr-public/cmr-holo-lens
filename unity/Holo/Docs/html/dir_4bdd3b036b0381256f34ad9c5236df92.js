var dir_4bdd3b036b0381256f34ad9c5236df92 =
[
    [ "IMixedRealityBaseInputHandler.cs", "_i_mixed_reality_base_input_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_base_input_handler.html", null ]
    ] ],
    [ "IMixedRealityDictationHandler.cs", "_i_mixed_reality_dictation_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_dictation_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_dictation_handler" ]
    ] ],
    [ "IMixedRealityFocusChangedHandler.cs", "_i_mixed_reality_focus_changed_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_focus_changed_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_focus_changed_handler" ]
    ] ],
    [ "IMixedRealityFocusHandler.cs", "_i_mixed_reality_focus_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_focus_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_focus_handler" ]
    ] ],
    [ "IMixedRealityGestureHandler.cs", "_i_mixed_reality_gesture_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_gesture_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_gesture_handler" ]
    ] ],
    [ "IMixedRealityHandJointHandler.cs", "_i_mixed_reality_hand_joint_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_joint_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_joint_handler" ]
    ] ],
    [ "IMixedRealityHandMeshHandler.cs", "_i_mixed_reality_hand_mesh_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandMeshHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_mesh_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_mesh_handler" ],
      [ "Microsoft.MixedReality.Toolkit.Input.HandMeshInfo", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_hand_mesh_info.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_hand_mesh_info" ]
    ] ],
    [ "IMixedRealityInputActionHandler.cs", "_i_mixed_reality_input_action_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputActionHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_action_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_action_handler" ]
    ] ],
    [ "IMixedRealityInputHandler.cs", "_i_mixed_reality_input_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_handler" ]
    ] ],
    [ "IMixedRealityPointerHandler.cs", "_i_mixed_reality_pointer_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_pointer_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_pointer_handler" ]
    ] ],
    [ "IMixedRealitySourcePoseHandler.cs", "_i_mixed_reality_source_pose_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_source_pose_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_source_pose_handler" ]
    ] ],
    [ "IMixedRealitySourceStateHandler.cs", "_i_mixed_reality_source_state_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_source_state_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_source_state_handler" ]
    ] ],
    [ "IMixedRealitySpeechHandler.cs", "_i_mixed_reality_speech_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_speech_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_speech_handler" ]
    ] ],
    [ "IMixedRealityTouchHandler.cs", "_i_mixed_reality_touch_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_touch_handler.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_touch_handler" ]
    ] ]
];