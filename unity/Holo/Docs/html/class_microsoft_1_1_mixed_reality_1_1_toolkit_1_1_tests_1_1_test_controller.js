var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller =
[
    [ "TestController", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#ae2978cbd8dd0a1e27995adf930679d07", null ],
    [ "Click", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a49c0edb5581f170874e890d60da95743", null ],
    [ "GetPointer< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a6ebf4ad9a4d033a6ab8a2e5331aefb6c", null ],
    [ "GetVelocity", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a6a9a8e6d547f283b5d205530e6dda423", null ],
    [ "Hide", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a56e8877ce4138c8ca809c078c4c169c9", null ],
    [ "Move", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a777131d03989ede653d0309661342b77", null ],
    [ "MoveTo", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#abf7df6c652488e1fd82e223cb5f5b11a", null ],
    [ "SetRotation", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a3913c4b86067d7eb50bafcb9aa1f60c0", null ],
    [ "Show", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#ab9d1e9436e3be77c54a14b0fec8a7ac2", null ],
    [ "handedness", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a338868bd1bf4e31e64cdd93e24c5ff59", null ],
    [ "position", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a68c18a12ef40146625ea10e5761cee4a", null ],
    [ "rotation", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#a867163ce1b047807eeb84827567449a9", null ],
    [ "simulationService", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html#aec6af6924e9fa2af72a1ffc3abfa2abd", null ]
];