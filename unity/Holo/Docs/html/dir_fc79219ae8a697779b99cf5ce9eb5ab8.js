var dir_fc79219ae8a697779b99cf5ce9eb5ab8 =
[
    [ "Distorters", "dir_3c7d166c679641afa8d67fb053f051ff.html", "dir_3c7d166c679641afa8d67fb053f051ff" ],
    [ "BaseRayStabilizer.cs", "_base_ray_stabilizer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.BaseRayStabilizer", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_base_ray_stabilizer.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_base_ray_stabilizer" ]
    ] ],
    [ "GazeStabilizer.cs", "_gaze_stabilizer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.GazeStabilizer", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_gaze_stabilizer.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_gaze_stabilizer" ]
    ] ],
    [ "InterpolationUtilities.cs", "_interpolation_utilities_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.InterpolationUtilities", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_interpolation_utilities.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_interpolation_utilities" ]
    ] ],
    [ "Interpolator.cs", "_interpolator_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.Interpolator", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_interpolator.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_interpolator" ]
    ] ],
    [ "MixedRealityRaycaster.cs", "_mixed_reality_raycaster_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.MixedRealityRaycaster", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_mixed_reality_raycaster.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_mixed_reality_raycaster" ]
    ] ],
    [ "RaycastResultComparer.cs", "_raycast_result_comparer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.RaycastResultComparer", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_raycast_result_comparer.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_raycast_result_comparer" ]
    ] ],
    [ "VectorRollingStatistics.cs", "_vector_rolling_statistics_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.VectorRollingStatistics", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_vector_rolling_statistics.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_vector_rolling_statistics" ]
    ] ]
];