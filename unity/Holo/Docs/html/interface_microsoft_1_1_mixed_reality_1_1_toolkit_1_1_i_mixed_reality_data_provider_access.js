var interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access =
[
    [ "GetDataProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access.html#a97f21c4882277bd2358b725fd95d32be", null ],
    [ "GetDataProvider< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access.html#a859968ba15430a72c34af5daa3857761", null ],
    [ "GetDataProviders", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access.html#a898be3e3c6bb00733ad39de8b381bc3d", null ],
    [ "GetDataProviders< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access.html#a93e4d8b421eacf6f28c7a52bce11f3d9", null ]
];