var struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands =
[
    [ "SpeechCommands", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands.html#a84a6b2a1ef40d90de440855041ac727b", null ],
    [ "Action", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands.html#a83acf7b6e4bb23523aac8481d917b6e8", null ],
    [ "KeyCode", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands.html#a8d7c8e5bed6be075bf62f3c51dc8b715", null ],
    [ "Keyword", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands.html#abb0d72d12d03c1d85e14b1e12f2b0d98", null ],
    [ "LocalizedKeyword", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands.html#a205bbc44b2012d2f6c55db3e880531f5", null ]
];