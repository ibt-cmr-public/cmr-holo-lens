var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest =
[
    [ "DeleteAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a70dfdc5299446067da09fb31bf2c9709", null ],
    [ "GetAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a59f5e39cfd382505451410162b448ba0", null ],
    [ "GetBasicAuthentication", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#af822d3fd406f46d1092d68f77c872564", null ],
    [ "GetBearerOAuthToken", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a34adecd85ec938c5bb80741dea6777e3", null ],
    [ "PostAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a7b6936a12e447d5729cb2ef72362d736", null ],
    [ "PostAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#aa2fddcf4daab1b6be9e0b6bfa5acd9b9", null ],
    [ "PostAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a6d43dfbdb2807d9aa6e2440b25180821", null ],
    [ "PostAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a0d461b583a85a265de442d5fa6fc363b", null ],
    [ "PutAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a80e8a1e35fc5acb286b96355a32b5f4d", null ],
    [ "PutAsync", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a848d389b416fd008729146f9fa4a057a", null ]
];