var dir_59742e9e4051c2d2558c7cb9ccc7b9f6 =
[
    [ "AnimatedCursorData.cs", "_animated_cursor_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.AnimatedCursorStateData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_animated_cursor_state_data.html", null ],
      [ "Microsoft.MixedReality.Toolkit.Input.AnimatedCursorContextData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_animated_cursor_context_data.html", null ],
      [ "Microsoft.MixedReality.Toolkit.Input.AnimatedCursorData< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_animated_cursor_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_animated_cursor_data" ]
    ] ],
    [ "CursorContextEnum.cs", "_cursor_context_enum_8cs.html", "_cursor_context_enum_8cs" ],
    [ "CursorStateEnum.cs", "_cursor_state_enum_8cs.html", "_cursor_state_enum_8cs" ],
    [ "InputActionEventPair.cs", "_input_action_event_pair_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionEventPair", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_event_pair.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_event_pair" ]
    ] ],
    [ "InputActionRuleDigital.cs", "_input_action_rule_digital_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionRuleDigital", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_digital.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_digital" ]
    ] ],
    [ "InputActionRuleDualAxis.cs", "_input_action_rule_dual_axis_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionRuleDualAxis", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_dual_axis.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_dual_axis" ]
    ] ],
    [ "InputActionRulePoseAxis.cs", "_input_action_rule_pose_axis_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionRulePoseAxis", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_pose_axis.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_pose_axis" ]
    ] ],
    [ "InputActionRuleQuaternionAxis.cs", "_input_action_rule_quaternion_axis_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionRuleQuaternionAxis", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_quaternion_axis.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_quaternion_axis" ]
    ] ],
    [ "InputActionRuleSingleAxis.cs", "_input_action_rule_single_axis_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionRuleSingleAxis", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_single_axis.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_single_axis" ]
    ] ],
    [ "InputActionRuleVectorAxis.cs", "_input_action_rule_vector_axis_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputActionRuleVectorAxis", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_vector_axis.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_rule_vector_axis" ]
    ] ],
    [ "KeywordAndResponse.cs", "_keyword_and_response_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.KeywordAndResponse", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_keyword_and_response.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_keyword_and_response" ]
    ] ],
    [ "MixedRealityGestureMapping.cs", "_mixed_reality_gesture_mapping_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityGestureMapping", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_gesture_mapping.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_gesture_mapping" ]
    ] ],
    [ "MixedRealityGesturesProfile.cs", "_mixed_reality_gestures_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityGesturesProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_gestures_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_gestures_profile" ]
    ] ],
    [ "MixedRealityInputAction.cs", "_mixed_reality_input_action_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_action.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_action" ]
    ] ],
    [ "MixedRealityInputActionRulesProfile.cs", "_mixed_reality_input_action_rules_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_action_rules_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_action_rules_profile" ]
    ] ],
    [ "MixedRealityInputActionsProfile.cs", "_mixed_reality_input_actions_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionsProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_actions_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_actions_profile" ]
    ] ],
    [ "MixedRealityInputSystemProfile.cs", "_mixed_reality_input_system_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_system_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_system_profile" ]
    ] ],
    [ "MixedRealityPointerProfile.cs", "_mixed_reality_pointer_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_pointer_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_pointer_profile" ]
    ] ],
    [ "MixedRealityRaycastHit.cs", "_mixed_reality_raycast_hit_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_raycast_hit.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_raycast_hit" ]
    ] ],
    [ "MixedRealitySpeechCommandsProfile.cs", "_mixed_reality_speech_commands_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealitySpeechCommandsProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_speech_commands_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_speech_commands_profile" ]
    ] ],
    [ "PointerBehavior.cs", "_pointer_behavior_8cs.html", "_pointer_behavior_8cs" ],
    [ "PointerOption.cs", "_pointer_option_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.PointerOption", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_pointer_option.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_pointer_option" ]
    ] ],
    [ "SpeechCommands.cs", "_speech_commands_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SpeechCommands", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_commands" ]
    ] ],
    [ "TouchableEventType.cs", "_touchable_event_type_8cs.html", "_touchable_event_type_8cs" ],
    [ "WindowsGestureSettings.cs", "_windows_gesture_settings_8cs.html", "_windows_gesture_settings_8cs" ]
];