var dir_519a9b7b016190e520c800007c65befd =
[
    [ "BaseInputEventData.cs", "_base_input_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.BaseInputEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_base_input_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_base_input_event_data" ]
    ] ],
    [ "DictationEventData.cs", "_dictation_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.DictationEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_dictation_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_dictation_event_data" ]
    ] ],
    [ "FocusEventData.cs", "_focus_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.FocusEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_focus_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_focus_event_data" ]
    ] ],
    [ "HandTrackingInputEventData.cs", "_hand_tracking_input_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_hand_tracking_input_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_hand_tracking_input_event_data" ]
    ] ],
    [ "InputEventData.cs", "_input_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputEventData< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_event_data" ]
    ] ],
    [ "MixedRealityPointerEventData.cs", "_mixed_reality_pointer_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_pointer_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_pointer_event_data" ]
    ] ],
    [ "SourcePoseEventData.cs", "_source_pose_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SourcePoseEventData< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_source_pose_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_source_pose_event_data" ]
    ] ],
    [ "SourceStateEventData.cs", "_source_state_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SourceStateEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_source_state_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_source_state_event_data" ]
    ] ],
    [ "SpeechEventData.cs", "_speech_event_data_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SpeechEventData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_event_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_event_data" ]
    ] ]
];