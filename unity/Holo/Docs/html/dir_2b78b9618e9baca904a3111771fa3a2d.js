var dir_2b78b9618e9baca904a3111771fa3a2d =
[
    [ "EditorClassExtensions", "dir_e096adee5c6f8dd0a83b0bf9fbc3d0bb.html", "dir_e096adee5c6f8dd0a83b0bf9fbc3d0bb" ],
    [ "AnimationCurveExtensions.cs", "_animation_curve_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.AnimationCurveExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_animation_curve_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_animation_curve_extensions" ]
    ] ],
    [ "ArrayExtensions.cs", "_array_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ArrayExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_array_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_array_extensions" ]
    ] ],
    [ "AssemblyExtensions.cs", "_assembly_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.AssemblyExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_assembly_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_assembly_extensions" ]
    ] ],
    [ "BoundsExtensions.cs", "_bounds_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.BoundsExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_bounds_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_bounds_extensions" ]
    ] ],
    [ "CameraExtensions.cs", "_camera_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.CameraExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_camera_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_camera_extensions" ]
    ] ],
    [ "CanvasExtensions.cs", "_canvas_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.CanvasExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions" ]
    ] ],
    [ "CollectionsExtensions.cs", "_collections_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.CollectionsExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions" ]
    ] ],
    [ "Color32Extensions.cs", "_color32_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Color32Extensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_color32_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_color32_extensions" ]
    ] ],
    [ "ComparerExtensions.cs", "_comparer_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ComparerExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_comparer_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_comparer_extensions" ]
    ] ],
    [ "ComponentExtensions.cs", "_component_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ComponentExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_component_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_component_extensions" ]
    ] ],
    [ "DateTimeExtensions.cs", "_date_time_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.DateTimeExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_date_time_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_date_time_extensions" ]
    ] ],
    [ "DoubleExtensions.cs", "_double_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.DoubleExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_double_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_double_extensions" ]
    ] ],
    [ "EnumerableExtensions.cs", "_enumerable_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.EnumerableExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_enumerable_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_enumerable_extensions" ]
    ] ],
    [ "EventSystemExtensions.cs", "_event_system_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.EventSystemExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_event_system_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_event_system_extensions" ]
    ] ],
    [ "FloatExtensions.cs", "_float_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.FloatExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_float_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_float_extensions" ]
    ] ],
    [ "GameObjectExtensions.cs", "_game_object_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.GameObjectExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_game_object_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_game_object_extensions" ]
    ] ],
    [ "HandednessExtensions.cs", "_handedness_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.HandednessExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_handedness_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_handedness_extensions" ]
    ] ],
    [ "LayerExtensions.cs", "_layer_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.LayerExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_layer_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_layer_extensions" ]
    ] ],
    [ "MathfExtensions.cs", "_mathf_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.MathExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_math_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_math_extensions" ]
    ] ],
    [ "ProcessExtensions.cs", "_process_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ProcessExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_process_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_process_extensions" ]
    ] ],
    [ "QuaternionExtensions.cs", "_quaternion_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.QuaternionExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_quaternion_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_quaternion_extensions" ]
    ] ],
    [ "RayExtensions.cs", "_ray_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.RayExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_ray_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_ray_extensions" ]
    ] ],
    [ "ReflectionExtensions.cs", "_reflection_extensions_8cs.html", null ],
    [ "StringBuilderExtensions.cs", "_string_builder_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.StringBuilderExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_string_builder_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_string_builder_extensions" ]
    ] ],
    [ "StringExtensions.cs", "_string_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.StringExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_string_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_string_extensions" ]
    ] ],
    [ "SystemNumericsExtensions.cs", "_system_numerics_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.SystemNumericsExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_system_numerics_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_system_numerics_extensions" ]
    ] ],
    [ "Texture2DExtensions.cs", "_texture2_d_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Texture2DExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_texture2_d_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_texture2_d_extensions" ]
    ] ],
    [ "TransformExtensions.cs", "_transform_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.TransformExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions" ]
    ] ],
    [ "TypeExtensions.cs", "_type_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.TypeExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_type_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_type_extensions" ]
    ] ],
    [ "UnityObjectExtensions.cs", "_unity_object_extensions_8cs.html", "_unity_object_extensions_8cs" ],
    [ "VectorExtensions.cs", "_vector_extensions_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.VectorExtensions", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_vector_extensions.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_vector_extensions" ]
    ] ]
];