var searchData=
[
  ['paginationmode_0',['PaginationMode',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_scrolling_object_collection.html#ad4da3d8fd23e44db3e9b6f5da6b2093a',1,'Microsoft::MixedReality::Toolkit::UI::ScrollingObjectCollection']]],
  ['performancetarget_1',['PerformanceTarget',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_optimize_window.html#aab7b28125bd175c366ed39b13a4a6af0',1,'Microsoft::MixedReality::Toolkit::Editor::MixedRealityOptimizeWindow']]],
  ['physicalpresseventbehavior_2',['PhysicalPressEventBehavior',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physical_press_event_router.html#a03d9ab781523c22bbc10d565fef123a4',1,'Microsoft::MixedReality::Toolkit::PhysicalPressEventRouter']]],
  ['pivotaxis_3',['PivotAxis',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities.html#a916f70a8a653bd0869febc6a82b3c586',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['plugintype_4',['PluginType',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build.html#ae6c229924936ec7da3b88745ef2478ee',1,'Microsoft::MixedReality::Toolkit::MSBuild']]],
  ['pointdistributionmode_5',['PointDistributionMode',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit.html#a608e40267af2875db93882208b60448a',1,'Microsoft::MixedReality::Toolkit']]],
  ['pointerbehavior_6',['PointerBehavior',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input.html#a3fbd86307f32b0b6915fd2c6b64b2065',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['profileclonebehavior_7',['ProfileCloneBehavior',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_profile_clone_window.html#a9f7e0c8dfb5847571157b628e38911e8',1,'Microsoft::MixedReality::Toolkit::Editor::MixedRealityProfileCloneWindow']]],
  ['progressindicatorstate_8',['ProgressIndicatorState',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i.html#add0ac7f7ae264189be2d2bd6c6f76dcb',1,'Microsoft::MixedReality::Toolkit::UI']]],
  ['projecttype_9',['ProjectType',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build.html#a05bd978f2611e2251b34c151c7097794',1,'Microsoft::MixedReality::Toolkit::MSBuild']]]
];
