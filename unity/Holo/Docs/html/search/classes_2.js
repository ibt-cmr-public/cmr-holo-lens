var searchData=
[
  ['cameracache_0',['CameraCache',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_camera_cache.html',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['cameraeventrouter_1',['CameraEventRouter',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_camera_event_router.html',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['cameraextensions_2',['CameraExtensions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_camera_extensions.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['camerafaderquad_3',['CameraFaderQuad',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_extensions_1_1_scene_transitions_1_1_camera_fader_quad.html',1,'Microsoft::MixedReality::Toolkit::Extensions::SceneTransitions']]],
  ['camerafovchecker_4',['CameraFOVChecker',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_camera_f_o_v_checker.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['camerasystemmanager_5',['CameraSystemManager',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_camera_system_1_1_camera_system_manager.html',1,'Microsoft::MixedReality::Toolkit::Experimental::CameraSystem']]],
  ['cameravideostreamframeheader_5ft_6',['CameraVideoStreamFrameHeader_t',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_camera_video_stream_frame_header__t.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['canvasextensions_7',['CanvasExtensions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_canvas_extensions.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['canvasutility_8',['CanvasUtility',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_utilities_1_1_canvas_utility.html',1,'Microsoft::MixedReality::Toolkit::Input::Utilities']]],
  ['capslockhighlight_9',['CapsLockHighlight',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_u_i_1_1_caps_lock_highlight.html',1,'Microsoft::MixedReality::Toolkit::Experimental::UI']]],
  ['charicon_10',['CharIcon',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_button_icon_set_1_1_char_icon.html',1,'Microsoft::MixedReality::Toolkit::UI::ButtonIconSet']]],
  ['clippingbox_11',['ClippingBox',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_clipping_box.html',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['clippingboxeditor_12',['ClippingBoxEditor',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_clipping_box_editor.html',1,'Microsoft::MixedReality::Toolkit::Editor']]],
  ['clippingplane_13',['ClippingPlane',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_clipping_plane.html',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['clippingplaneeditor_14',['ClippingPlaneEditor',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_clipping_plane_editor.html',1,'Microsoft::MixedReality::Toolkit::Editor']]],
  ['clippingplanerenderer_15',['ClippingPlaneRenderer',['../class_clipping_plane_renderer.html',1,'']]],
  ['clippingprimitive_16',['ClippingPrimitive',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_clipping_primitive.html',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['clippingprimitiveeditor_17',['ClippingPrimitiveEditor',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_clipping_primitive_editor.html',1,'Microsoft::MixedReality::Toolkit::Editor']]],
  ['clippingsphere_18',['ClippingSphere',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_clipping_sphere.html',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['clippingsphereeditor_19',['ClippingSphereEditor',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_clipping_sphere_editor.html',1,'Microsoft::MixedReality::Toolkit::Editor']]],
  ['collectionsextensions_20',['CollectionsExtensions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_collections_extensions.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['collidernearinteractiontouchable_21',['ColliderNearInteractionTouchable',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_collider_near_interaction_touchable.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['color32extensions_22',['Color32Extensions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_color32_extensions.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['colormap_23',['ColorMap',['../class_color_map.html',1,'']]],
  ['colorpicker_24',['ColorPicker',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_color_picker_1_1_color_picker.html',1,'Microsoft::MixedReality::Toolkit::Experimental::ColorPicker']]],
  ['comparableraycastresult_25',['ComparableRaycastResult',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_comparable_raycast_result.html',1,'Microsoft::MixedReality::Toolkit::Physics']]],
  ['comparerextensions_26',['ComparerExtensions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_comparer_extensions.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['compilationplatforminfo_27',['CompilationPlatformInfo',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build_1_1_compilation_platform_info.html',1,'Microsoft::MixedReality::Toolkit::MSBuild']]],
  ['componentextensions_28',['ComponentExtensions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_component_extensions.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['compositor_5fcumulativestats_29',['Compositor_CumulativeStats',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_compositor___cumulative_stats.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['compositor_5fframetiming_30',['Compositor_FrameTiming',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_compositor___frame_timing.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['compositor_5foverlaysettings_31',['Compositor_OverlaySettings',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_compositor___overlay_settings.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['constantviewsize_32',['ConstantViewSize',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_solvers_1_1_constant_view_size.html',1,'Microsoft::MixedReality::Toolkit::Utilities::Solvers']]],
  ['constraintmanager_33',['ConstraintManager',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_constraint_manager.html',1,'Microsoft::MixedReality::Toolkit::UI']]],
  ['constraintmanagerinspector_34',['ConstraintManagerInspector',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_constraint_manager_inspector.html',1,'Microsoft::MixedReality::Toolkit::Editor']]],
  ['constructgltf_35',['ConstructGltf',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_gltf_1_1_serialization_1_1_construct_gltf.html',1,'Microsoft::MixedReality::Toolkit::Utilities::Gltf::Serialization']]],
  ['controllerfinder_36',['ControllerFinder',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_solvers_1_1_controller_finder.html',1,'Microsoft::MixedReality::Toolkit::Utilities::Solvers']]],
  ['controllerfinderinspector_37',['ControllerFinderInspector',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_solvers_1_1_controller_finder_inspector.html',1,'Microsoft::MixedReality::Toolkit::Utilities::Editor::Solvers']]],
  ['controllerinputactionoption_38',['ControllerInputActionOption',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_editor_1_1_controller_input_action_option.html',1,'Microsoft::MixedReality::Toolkit::Input::Editor']]],
  ['controllerinputactionoptions_39',['ControllerInputActionOptions',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_editor_1_1_controller_input_action_options.html',1,'Microsoft::MixedReality::Toolkit::Input::Editor']]],
  ['controllermappinglibrary_40',['ControllerMappingLibrary',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_controller_mapping_library.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['controllerpopupwindow_41',['ControllerPopupWindow',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_controller_popup_window.html',1,'Microsoft::MixedReality::Toolkit::Editor']]],
  ['controllerposesynchronizer_42',['ControllerPoseSynchronizer',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_controller_pose_synchronizer.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['controllerposesynchronizerinspector_43',['ControllerPoseSynchronizerInspector',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_editor_1_1_controller_pose_synchronizer_inspector.html',1,'Microsoft::MixedReality::Toolkit::Input::Editor']]],
  ['copenvrcontext_44',['COpenVRContext',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_open_v_r_context.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['coreservices_45',['CoreServices',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_core_services.html',1,'Microsoft::MixedReality::Toolkit']]],
  ['csprojectdependency_46',['CSProjectDependency',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build_1_1_c_s_project_dependency.html',1,'Microsoft::MixedReality::Toolkit::MSBuild']]],
  ['csprojectinfo_47',['CSProjectInfo',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build_1_1_c_s_project_info.html',1,'Microsoft::MixedReality::Toolkit::MSBuild']]],
  ['cursorcontextinfo_48',['CursorContextInfo',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_cursor_context_info.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['cursorcontextmanipulationhandler_49',['CursorContextManipulationHandler',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_cursor_context_manipulation_handler.html',1,'Microsoft::MixedReality::Toolkit::UI']]],
  ['cursorcontextobjectmanipulator_50',['CursorContextObjectManipulator',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_cursor_context_object_manipulator.html',1,'Microsoft::MixedReality::Toolkit::UI']]],
  ['cursormodifier_51',['CursorModifier',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_cursor_modifier.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['curvepointer_52',['CurvePointer',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_curve_pointer.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['customusages_53',['CustomUsages',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_x_r_s_d_k_1_1_input_1_1_custom_usages.html',1,'Microsoft::MixedReality::Toolkit::XRSDK::Input']]],
  ['cvrapplications_54',['CVRApplications',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_applications.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrchaperone_55',['CVRChaperone',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_chaperone.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrchaperonesetup_56',['CVRChaperoneSetup',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_chaperone_setup.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrcompositor_57',['CVRCompositor',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_compositor.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrdrivermanager_58',['CVRDriverManager',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_driver_manager.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrextendeddisplay_59',['CVRExtendedDisplay',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_extended_display.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrinput_60',['CVRInput',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_input.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvriobuffer_61',['CVRIOBuffer',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_i_o_buffer.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrnotifications_62',['CVRNotifications',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_notifications.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvroverlay_63',['CVROverlay',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_overlay.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrrendermodels_64',['CVRRenderModels',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_render_models.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrresources_65',['CVRResources',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_resources.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrscreenshots_66',['CVRScreenshots',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_screenshots.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrsettinghelper_67',['CVRSettingHelper',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_setting_helper.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrsettings_68',['CVRSettings',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_settings.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrspatialanchors_69',['CVRSpatialAnchors',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_spatial_anchors.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrsystem_70',['CVRSystem',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_system.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]],
  ['cvrtrackedcamera_71',['CVRTrackedCamera',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_tracked_camera.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]]
];
