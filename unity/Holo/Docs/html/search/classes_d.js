var searchData=
[
  ['nearinteractiongrabbable_0',['NearInteractionGrabbable',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_grabbable.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchable_1',['NearInteractionTouchable',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchableinspector_2',['NearInteractionTouchableInspector',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable_inspector.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchableinspectorbase_3',['NearInteractionTouchableInspectorBase',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable_inspector_base.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchablesurface_4',['NearInteractionTouchableSurface',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable_surface.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchableunityui_5',['NearInteractionTouchableUnityUI',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable_unity_u_i.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchablevolume_6',['NearInteractionTouchableVolume',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable_volume.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['nearinteractiontouchablevolumeinspector_7',['NearInteractionTouchableVolumeInspector',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_near_interaction_touchable_volume_inspector.html',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['networkdiscoverywithanchors_8',['NetworkDiscoveryWithAnchors',['../class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_network_discovery_with_anchors.html',1,'HoloToolkit::Examples::SharingWithUNET']]],
  ['networkinterfaces_9',['NetworkInterfaces',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_windows_device_portal_1_1_network_interfaces.html',1,'Microsoft::MixedReality::Toolkit::WindowsDevicePortal']]],
  ['networkprofileinfo_10',['NetworkProfileInfo',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_windows_device_portal_1_1_network_profile_info.html',1,'Microsoft::MixedReality::Toolkit::WindowsDevicePortal']]],
  ['nonnativekeyboard_11',['NonNativeKeyboard',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_u_i_1_1_non_native_keyboard.html',1,'Microsoft::MixedReality::Toolkit::Experimental::UI']]],
  ['nonnativekeyboardtouchassistant_12',['NonNativeKeyboardTouchAssistant',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_u_i_1_1_non_native_keyboard_touch_assistant.html',1,'Microsoft::MixedReality::Toolkit::Experimental::UI']]],
  ['notificationbitmap_5ft_13',['NotificationBitmap_t',['../struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_notification_bitmap__t.html',1,'Microsoft::MixedReality::Toolkit::OpenVR::Headers']]]
];
