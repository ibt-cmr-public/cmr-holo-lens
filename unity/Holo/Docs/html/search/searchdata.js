var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvwxyz",
  1: "abcdefghijklmnopqrstuvwx",
  2: "ahm",
  3: "abcdefghijklmnopqrstuvwx",
  4: "abcdefghijklmnopqrstuvwxy",
  5: "abcdefghijklmnopqrstuvwxyz",
  6: "adefimoprtuv",
  7: "abcdefghijklmoprstvw",
  8: "abcdefghijklmnopqrstuvwxyz",
  9: "abcdefghijklmnopqrstuvwz",
  10: "cdiosu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties",
  10: "Events"
};

