var searchData=
[
  ['targetframework_0',['TargetFramework',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build.html#a081cd71e3671fde4f02ef6051dba32df',1,'Microsoft::MixedReality::Toolkit::MSBuild']]],
  ['teleportsurfaceresult_1',['TeleportSurfaceResult',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics.html#a748a4e51aa2396e4568e2e6a182a4d24',1,'Microsoft::MixedReality::Toolkit::Physics']]],
  ['themepropertytypes_2',['ThemePropertyTypes',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i.html#a27234f80cbbf6f95f92d770e9b90b209',1,'Microsoft::MixedReality::Toolkit::UI']]],
  ['toolmode_3',['ToolMode',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_simulation_window.html#a023ed62251dd9533a68f421640634063',1,'Microsoft::MixedReality::Toolkit::Input::InputSimulationWindow']]],
  ['tooltipattachpoint_4',['ToolTipAttachPoint',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i.html#a8ec41da48d0ef7afb590eab9da9d1f14',1,'Microsoft::MixedReality::Toolkit::UI']]],
  ['touchableeventtype_5',['TouchableEventType',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input.html#add012eef68003411c2846f5cacc45b55',1,'Microsoft::MixedReality::Toolkit::Input']]],
  ['trackedhandjoint_6',['TrackedHandJoint',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities.html#a4f689b5ed6e86f702daa9448ae92cb52',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['trackedobjecttype_7',['TrackedObjectType',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities.html#a57de77da48ec697d316b94a1180bebeb',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['trackingstate_8',['TrackingState',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit.html#ab1cf1de32f30772290ecb6397f439652',1,'Microsoft::MixedReality::Toolkit']]],
  ['transformationstate_9',['TransformationState',['../class_model_with_plate.html#a7f5c7dea9ae0bece0489faad295e2fe4',1,'ModelWithPlate']]],
  ['transformflags_10',['TransformFlags',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities.html#a8fa928814cc97d0cea48e2233bf33c2d',1,'Microsoft::MixedReality::Toolkit::Utilities']]],
  ['twohandedmanipulation_11',['TwoHandedManipulation',['../class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_manipulation_handler.html#ae34d0a19497b90df148a76eeaee8e030',1,'Microsoft::MixedReality::Toolkit::UI::ManipulationHandler']]],
  ['typegrouping_12',['TypeGrouping',['../namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities.html#aac211833ce7aed2dbc5f2453d4ae6aff',1,'Microsoft::MixedReality::Toolkit::Utilities']]]
];
