var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities =
[
    [ "AssertAboutEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#ac67e9c923f455972a8472ac012ce8168", null ],
    [ "AssertAboutEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a018e17bd6938f7ad9bd5e3fd5be68557", null ],
    [ "AssertGreaterOrEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a2e5cbacb02c6ee349acbd0a363ef1003", null ],
    [ "AssertGreaterOrEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a31316efa605bfc0a9bd4e6cb67cecf2b", null ],
    [ "AssertLessOrEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a9f21deafb444ed7c16985612828f9371", null ],
    [ "AssertLessOrEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a7029e971280e0b23600b570abffdeacc", null ],
    [ "AssertNotAboutEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a87bce1a15f5940a54e7440d027cc3f29", null ],
    [ "AssertNotAboutEqual", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#af8d7e6954c54e1a32d236d3221586633", null ],
    [ "DirectionRelativeToPlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#aa6ffd9d8566da23cb4ba558ac0e150f0", null ],
    [ "EditorCreateScenes", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a374b0f6ecfaf8063b60d8b9963330276", null ],
    [ "EditorTearDownScenes", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#ae672fa0a3b412dc62ad41b724cfeaae4", null ],
    [ "GetDefaultMixedRealityProfile< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a3b88e0c7de43fa5da43aa6521f72e664", null ],
    [ "InitializeCamera", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#ac81c5b0a503c6355490496e9ab3ac3ed", null ],
    [ "InitializeMixedRealityToolkit", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#af1ab204a43d79e291fe2e17ee5126fa9", null ],
    [ "InitializeMixedRealityToolkit", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#af36000121a591223e68fb94996fc1470", null ],
    [ "InitializeMixedRealityToolkitAndCreateScenes", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a74bea47daaccc3e80583f87a481d6006", null ],
    [ "InitializePlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#ab3cb453494376383c38a8944d08b4f4e", null ],
    [ "PlaceRelativeToPlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a9af3d799ed894c179e90eff6166006a2", null ],
    [ "PlaceRelativeToPlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#aa9f7f5b333d650522208d839e5651fd5", null ],
    [ "PlaceRelativeToPlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a43c6cab277ce4c1a9193eb1e1f4cab39", null ],
    [ "PlayspaceToArbitraryPose", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#abecb1267616c75a924197db69d3f851e", null ],
    [ "PlayspaceToOriginLookingForward", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#ada98de80c9354bf0ac85124ac0d6c8f7", null ],
    [ "PlayspaceToPositionAndRotation", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a0bcbdd052e4d0d12f6c4669fcfc6ba3f", null ],
    [ "PositionRelativeToPlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a708e2e004eb37564a302cca48941ae3a", null ],
    [ "RotationRelativeToPlayspace", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#ab14331d5f8189ffe6e27daf0835a7fd0", null ],
    [ "ShutdownMixedRealityToolkit", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a00dca53fb0eb777b32c6b70e59b5c971", null ],
    [ "additiveTestScenes", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a92b3e9a879482064f39fb1e24c5b2feb", null ],
    [ "primaryTestScene", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a5d4a9c9b4483fc61d0404d15293b453f", null ],
    [ "ArbitraryParentPose", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a687dfd790786d3e19a07175e99387312", null ],
    [ "ArbitraryPlayspacePose", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a62cf29e930ac2c737c962b04506a3bf4", null ]
];