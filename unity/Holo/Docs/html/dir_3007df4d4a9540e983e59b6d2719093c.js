var dir_3007df4d4a9540e983e59b6d2719093c =
[
    [ "DocLinkAttribute.cs", "_doc_link_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.DocLinkAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_doc_link_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_doc_link_attribute" ]
    ] ],
    [ "EnumFlagsAttribute.cs", "_enum_flags_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.EnumFlagsAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_enum_flags_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_enum_flags_attribute" ]
    ] ],
    [ "ExperimentalAttribute.cs", "_experimental_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ExperimentalAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_attribute" ]
    ] ],
    [ "ExtendsAttribute.cs", "_extends_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ExtendsAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_extends_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_extends_attribute" ]
    ] ],
    [ "HelpAttribute.cs", "_help_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.HelpAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_help_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_help_attribute" ]
    ] ],
    [ "ImplementsAttribute.cs", "_implements_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ImplementsAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_implements_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_implements_attribute" ]
    ] ],
    [ "MixedRealityControllerAttribute.cs", "_mixed_reality_controller_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityControllerAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_controller_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_controller_attribute" ]
    ] ],
    [ "MixedRealityDataProviderAttribute.cs", "_mixed_reality_data_provider_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.MixedRealityDataProviderAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_data_provider_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_data_provider_attribute" ]
    ] ],
    [ "MixedRealityExtensionServiceAttribute.cs", "_mixed_reality_extension_service_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.MixedRealityExtensionServiceAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_extension_service_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_extension_service_attribute" ]
    ] ],
    [ "MixedRealityServiceInspectorAttribute.cs", "_mixed_reality_service_inspector_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.MixedRealityServiceInspectorAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_service_inspector_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_service_inspector_attribute" ]
    ] ],
    [ "MixedRealityServiceProfileAttribute.cs", "_mixed_reality_service_profile_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.MixedRealityServiceProfileAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_service_profile_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_service_profile_attribute" ]
    ] ],
    [ "PhysicsLayerAttribute.cs", "_physics_layer_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Physics.PhysicsLayerAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_physics_layer_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_physics_layer_attribute" ]
    ] ],
    [ "PrefabAttribute.cs", "_prefab_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.PrefabAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_prefab_attribute.html", null ]
    ] ],
    [ "ReadOnlyAttribute.cs", "_read_only_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ReadOnlyAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_read_only_attribute.html", null ],
      [ "Microsoft.MixedReality.Toolkit.BeginReadOnlyGroupAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_begin_read_only_group_attribute.html", null ],
      [ "Microsoft.MixedReality.Toolkit.EndReadOnlyGroupAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_end_read_only_group_attribute.html", null ]
    ] ],
    [ "ScenePickAttribute.cs", "_scene_pick_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.ScenePickAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_scene_pick_attribute.html", null ]
    ] ],
    [ "SystemTypeAttribute.cs", "_system_type_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.SystemTypeAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_system_type_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_system_type_attribute" ]
    ] ],
    [ "TagPropertyAttribute.cs", "_tag_property_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.TagPropertyAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tag_property_attribute.html", null ]
    ] ],
    [ "Vector3RangeAttribute.cs", "_vector3_range_attribute_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Vector3RangeAttribute", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_vector3_range_attribute.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_vector3_range_attribute" ]
    ] ]
];