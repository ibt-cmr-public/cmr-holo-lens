var dir_3285c70b37ddb9cba1e592b3ede87ac6 =
[
    [ "DwellHandler.cs", "_dwell_handler_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Experimental.Dwell.DwellHandler", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_dwell_1_1_dwell_handler.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_dwell_1_1_dwell_handler" ]
    ] ],
    [ "DwellProfile.cs", "_dwell_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Experimental.Dwell.DwellProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_dwell_1_1_dwell_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_dwell_1_1_dwell_profile" ]
    ] ],
    [ "DwellUnityEvent.cs", "_dwell_unity_event_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Experimental.Dwell.DwellUnityEvent", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_dwell_1_1_dwell_unity_event.html", null ]
    ] ]
];