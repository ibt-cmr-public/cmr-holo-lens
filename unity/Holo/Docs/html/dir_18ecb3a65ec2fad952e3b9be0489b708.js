var dir_18ecb3a65ec2fad952e3b9be0489b708 =
[
    [ "EditorPreferences.cs", "_editor_preferences_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Utilities.Editor.EditorPreferences", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_editor_preferences.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_editor_preferences" ]
    ] ],
    [ "MixedRealityProjectPreferences.cs", "_mixed_reality_project_preferences_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Editor.MixedRealityProjectPreferences", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_project_preferences.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_project_preferences" ]
    ] ],
    [ "ProjectPreferences.cs", "_project_preferences_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Utilities.Editor.ProjectPreferences", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_project_preferences.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_project_preferences" ]
    ] ],
    [ "SerializableDictionary.cs", "_serializable_dictionary_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Utilities.SerializableDictionary< TKey, TValue >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_serializable_dictionary.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_serializable_dictionary" ]
    ] ]
];