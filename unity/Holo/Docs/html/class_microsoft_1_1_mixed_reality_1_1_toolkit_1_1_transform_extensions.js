var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions =
[
    [ "EnumerateAncestors", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a5ff99f536548e774498676f00d824acc", null ],
    [ "EnumerateHierarchy", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#ad29e6a318f904f76194f2c00456af6b8", null ],
    [ "EnumerateHierarchy", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a9d2dcb32546c11e796b8df375db69eea", null ],
    [ "FindAncestorComponent< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a0ca5a227dd9e2bbf47d0b63fc5efc358", null ],
    [ "GetChildRecursive", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a35e4f0b1fedd7b1ba279c7e3c077b79a", null ],
    [ "GetColliderBounds", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a88e410f7ac694e2ca22fed285eab7442", null ],
    [ "GetDepth", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a0ebb561e5e4ac6e12f0323eaf3a58539", null ],
    [ "GetFullPath", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#acc16ad90887bf291c8521fdeb46db627", null ],
    [ "InverseTransformSize", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a3be70e758a68752ece74e4e3153f8087", null ],
    [ "IsParentOrChildOf", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#a22799cc8a4b7c79f44b1b6e522ffd637", null ],
    [ "TransformSize", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#af9dee5f14b415b2ffbf820f7dcb42ae8", null ],
    [ "TryGetDepth", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_transform_extensions.html#ae0003f08337329fb9666038cc90a1a45", null ]
];