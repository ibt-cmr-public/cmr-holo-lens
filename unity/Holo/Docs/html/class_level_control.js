var class_level_control =
[
    [ "ImmersedAvatarPathInfo", "class_level_control_1_1_immersed_avatar_path_info.html", "class_level_control_1_1_immersed_avatar_path_info" ],
    [ "LevelPlayerStateData", "class_level_control_1_1_level_player_state_data.html", "class_level_control_1_1_level_player_state_data" ],
    [ "GetCurrentTransform", "class_level_control.html#ab510904efd2350574c07ccdebb48f3e4", null ],
    [ "IsImmersed", "class_level_control.html#a6c7108c858ec33b23b08d4ea74210d5e", null ],
    [ "RemoteAvatarReady", "class_level_control.html#ab68e6ca8ac8c4f362bc69ff4c42bb20d", null ],
    [ "SetRemoteAvatarLevelPosition", "class_level_control.html#a31adad23479511c0070bf4fb56a6782e", null ],
    [ "AvatarStuff", "class_level_control.html#a64301b37bb86c0021bccfeb27bfd8e46", null ],
    [ "EnableCollaboration", "class_level_control.html#a234d68b8a432d4f3cf178f0fa0c9ac2a", null ],
    [ "GazeIndicatorPrefab", "class_level_control.html#a939ae0d4afbf9069b2b0c7ab1c076e4c", null ],
    [ "GiantAvatar", "class_level_control.html#a9428647e9f48fec87960427326f455d2", null ],
    [ "ImmersiveScale", "class_level_control.html#a05166dced22b862e3c54fe84f911a109", null ],
    [ "ParentObject", "class_level_control.html#a1246113751e1ee4b8aeed405412e62dd", null ],
    [ "SafetyColliders", "class_level_control.html#a516dedd6f4adfba3208ad5c19f62cdbb", null ],
    [ "Immersed", "class_level_control.html#ac7cde037bfc2df1b0864175a6cac763c", null ],
    [ "Instance", "class_level_control.html#aa9a9d56520f2802d8a182f797a8cddcf", null ]
];