var class_models_collection =
[
    [ "BundleCaption", "class_models_collection.html#a1fddc85e638b887dd96da6da003f97f0", null ],
    [ "BundleIcon", "class_models_collection.html#a0d75a5b90c729297b140a3a2c8097403", null ],
    [ "BundleLoad", "class_models_collection.html#a18d6525dfe2e312f036f65ab6a1d6cd2", null ],
    [ "BundleName", "class_models_collection.html#a480b20bdbf842ac146feacdb04f7c162", null ],
    [ "FindBundle", "class_models_collection.html#af635c1441211996491dc1fbf90c3a533", null ],
    [ "BundlesCount", "class_models_collection.html#a502af141394bce3d9ad7a1d7c06e2e0e", null ],
    [ "Singleton", "class_models_collection.html#a03124844288478a7767da04c74c2f6d7", null ]
];