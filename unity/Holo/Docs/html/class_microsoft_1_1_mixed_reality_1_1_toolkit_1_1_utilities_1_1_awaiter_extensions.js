var class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions =
[
    [ "SimpleCoroutineAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions_1_1_simple_coroutine_awaiter.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions_1_1_simple_coroutine_awaiter" ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a6d4891e98eb945b35ddf93c410f56177", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a3910ade54305c913699ce9ec4c6030c9", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#aee34922be432e0e221d6dbfbeac666de", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a3264b60721bd6a7a800debbe85e7f49c", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a14bea294efa9b142ad4a5e3e82c55385", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a4c19605845849643ed9e73191f7639fb", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#aabde08543f9a0158b698f5848aa4eed1", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a62eb6396970f2206eb1d05b9e3f8b656", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a113fd6a5e8c2263f65558ed78a7c1f50", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a165ec40cf3b31705a83242d7e6dd011d", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a921fc626099782803ec9776e9c90c1eb", null ],
    [ "GetAwaiter", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#ae8636692346bbec7d640059aeb21f945", null ],
    [ "GetAwaiter< T >", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_awaiter_extensions.html#a979acbfbaec60f7b9989ba39e2a7d254", null ]
];