var dir_f78429847844f3af83677dbfbe1d9ebc =
[
    [ "IMixedRealityController.cs", "_i_mixed_reality_controller_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityController", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_controller.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_controller" ]
    ] ],
    [ "IMixedRealityControllerPoseSynchronizer.cs", "_i_mixed_reality_controller_pose_synchronizer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityControllerPoseSynchronizer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_controller_pose_synchronizer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_controller_pose_synchronizer" ]
    ] ],
    [ "IMixedRealityControllerVisualizer.cs", "_i_mixed_reality_controller_visualizer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityControllerVisualizer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_controller_visualizer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_controller_visualizer" ]
    ] ],
    [ "IMixedRealityDictationSystem.cs", "_i_mixed_reality_dictation_system_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationSystem", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_dictation_system.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_dictation_system" ]
    ] ],
    [ "IMixedRealityHand.cs", "_i_mixed_reality_hand_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityHand", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand" ]
    ] ],
    [ "IMixedRealityHandJointService.cs", "_i_mixed_reality_hand_joint_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointService", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_joint_service.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_joint_service" ]
    ] ],
    [ "IMixedRealityHandVisualizer.cs", "_i_mixed_reality_hand_visualizer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandVisualizer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_visualizer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_hand_visualizer" ]
    ] ],
    [ "IMixedRealityInputDeviceManager.cs", "_i_mixed_reality_input_device_manager_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputDeviceManager", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_device_manager.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_device_manager" ]
    ] ],
    [ "IMixedRealitySpeechSystem.cs", "_i_mixed_reality_speech_system_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechSystem", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_speech_system.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_speech_system" ]
    ] ]
];