var dir_cf927571950d27924da62673920bd72f =
[
    [ "IMixedRealityCapabilityCheck.cs", "_i_mixed_reality_capability_check_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.IMixedRealityCapabilityCheck", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_capability_check.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_capability_check" ]
    ] ],
    [ "IMixedRealityDataProvider.cs", "_i_mixed_reality_data_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.IMixedRealityDataProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider.html", null ]
    ] ],
    [ "IMixedRealityDataProviderAccess.cs", "_i_mixed_reality_data_provider_access_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.IMixedRealityDataProviderAccess", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access" ]
    ] ],
    [ "IMixedRealityExtensionService.cs", "_i_mixed_reality_extension_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.IMixedRealityExtensionService", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_extension_service.html", null ]
    ] ],
    [ "IMixedRealityService.cs", "_i_mixed_reality_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.IMixedRealityService", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_service" ]
    ] ]
];