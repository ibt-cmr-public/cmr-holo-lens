var dir_dae2b88d152cab8b3b589a726ae74270 =
[
    [ "Editor", "dir_a8e0d48c939c32a7345567ae497ec965.html", "dir_a8e0d48c939c32a7345567ae497ec965" ],
    [ "AssemblyInfo.cs", "_core_2_providers_2_input_simulation_2_assembly_info_8cs.html", null ],
    [ "BaseInputSimulationService.cs", "_base_input_simulation_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_base_input_simulation_service.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_base_input_simulation_service" ]
    ] ],
    [ "IInputSimulationService.cs", "_i_input_simulation_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IInputSimulationService", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_input_simulation_service.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_input_simulation_service" ]
    ] ],
    [ "IMixedRealityInputPlaybackService.cs", "_i_mixed_reality_input_playback_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_playback_service.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_playback_service" ]
    ] ],
    [ "InputPlaybackService.cs", "_input_playback_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.InputPlaybackService", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_playback_service.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_playback_service" ]
    ] ],
    [ "InputSimulationEnum.cs", "_input_simulation_enum_8cs.html", "_input_simulation_enum_8cs" ],
    [ "InputSimulationHelpGuide.cs", "_input_simulation_help_guide_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Examples.InputSimulationHelpGuide", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_examples_1_1_input_simulation_help_guide.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_examples_1_1_input_simulation_help_guide" ]
    ] ],
    [ "InputSimulationService.cs", "_input_simulation_service_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MouseDelta", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mouse_delta.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mouse_delta" ],
      [ "Microsoft.MixedReality.Toolkit.Input.InputSimulationService", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_simulation_service.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_simulation_service" ]
    ] ],
    [ "KeyBinding.cs", "_key_binding_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.KeyBinding", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_key_binding.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_key_binding" ],
      [ "Microsoft.MixedReality.Toolkit.Input.KeyInputSystem", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_key_input_system.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_key_input_system" ]
    ] ],
    [ "ManualCameraControl.cs", "_manual_camera_control_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.ManualCameraControl", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_manual_camera_control.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_manual_camera_control" ]
    ] ],
    [ "MixedRealityInputSimulationProfile.cs", "_mixed_reality_input_simulation_profile_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_simulation_profile.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_simulation_profile" ]
    ] ],
    [ "MouseRotationProvider.cs", "_mouse_rotation_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mouse_rotation_provider.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mouse_rotation_provider" ]
    ] ],
    [ "SimulatedArticulatedHand.cs", "_simulated_articulated_hand_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_articulated_hand.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_articulated_hand" ]
    ] ],
    [ "SimulatedArticulatedHandPoses.cs", "_simulated_articulated_hand_poses_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_articulated_hand_poses.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_articulated_hand_poses" ]
    ] ],
    [ "SimulatedControllerDataProvider.cs", "_simulated_controller_data_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_controller_data_provider.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_controller_data_provider" ]
    ] ],
    [ "SimulatedGestureHand.cs", "_simulated_gesture_hand_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_gesture_hand.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_gesture_hand" ]
    ] ],
    [ "SimulatedHand.cs", "_simulated_hand_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedHandData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand_data" ],
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedHand", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand" ]
    ] ],
    [ "SimulatedHandDataProvider.cs", "_simulated_hand_data_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand_data_provider.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand_data_provider" ]
    ] ],
    [ "SimulatedHandUtils.cs", "_simulated_hand_utils_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand_utils.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_hand_utils" ]
    ] ],
    [ "SimulatedMotionController.cs", "_simulated_motion_controller_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller_data.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller_data" ],
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedMotionController", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller" ]
    ] ],
    [ "SimulatedMotionControllerButtonState.cs", "_simulated_motion_controller_button_state_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller_button_state.html", "struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller_button_state" ]
    ] ],
    [ "SimulatedMotionControllerDataProvider.cs", "_simulated_motion_controller_data_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller_data_provider.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_simulated_motion_controller_data_provider" ]
    ] ]
];