var dir_3ca789e0cea34b0d54d381d012f60f53 =
[
    [ "Handlers", "dir_4bdd3b036b0381256f34ad9c5236df92.html", "dir_4bdd3b036b0381256f34ad9c5236df92" ],
    [ "ICursorModifier.cs", "_i_cursor_modifier_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.ICursorModifier", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_cursor_modifier.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_cursor_modifier" ]
    ] ],
    [ "IInputActionRule.cs", "_i_input_action_rule_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IInputActionRule< T >", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_input_action_rule.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_input_action_rule" ]
    ] ],
    [ "IMixedRealityCursor.cs", "_i_mixed_reality_cursor_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityCursor", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_cursor.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_cursor" ]
    ] ],
    [ "IMixedRealityEyeGazeDataProvider.cs", "_i_mixed_reality_eye_gaze_data_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_eye_gaze_data_provider.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_eye_gaze_data_provider" ]
    ] ],
    [ "IMixedRealityEyeGazeProvider.cs", "_i_mixed_reality_eye_gaze_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_eye_gaze_provider.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_eye_gaze_provider" ]
    ] ],
    [ "IMixedRealityEyeSaccadeProvider.cs", "_i_mixed_reality_eye_saccade_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_eye_saccade_provider.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_eye_saccade_provider" ]
    ] ],
    [ "IMixedRealityFocusProvider.cs", "_i_mixed_reality_focus_provider_8cs.html", "_i_mixed_reality_focus_provider_8cs" ],
    [ "IMixedRealityGazeProvider.cs", "_i_mixed_reality_gaze_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityGazeProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_gaze_provider.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_gaze_provider" ]
    ] ],
    [ "IMixedRealityGazeProviderHeadOverride.cs", "_i_mixed_reality_gaze_provider_head_override_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityGazeProviderHeadOverride", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_gaze_provider_head_override.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_gaze_provider_head_override" ]
    ] ],
    [ "IMixedRealityInputSource.cs", "_i_mixed_reality_input_source_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_source.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_source" ]
    ] ],
    [ "IMixedRealityInputSystem.cs", "_i_mixed_reality_input_system_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_system.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_system" ]
    ] ],
    [ "IMixedRealityMouseDeviceManager.cs", "_i_mixed_reality_mouse_device_manager_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityMouseDeviceManager", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_mouse_device_manager.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_mouse_device_manager" ]
    ] ],
    [ "IMixedRealityMousePointer.cs", "_i_mixed_reality_mouse_pointer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityMousePointer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_mouse_pointer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_mouse_pointer" ]
    ] ],
    [ "IMixedRealityNearPointer.cs", "_i_mixed_reality_near_pointer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityNearPointer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_near_pointer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_near_pointer" ]
    ] ],
    [ "IMixedRealityPointer.cs", "_i_mixed_reality_pointer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_pointer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_pointer" ]
    ] ],
    [ "IMixedRealityPointerMediator.cs", "_i_mixed_reality_pointer_mediator_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerMediator", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_pointer_mediator.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_pointer_mediator" ]
    ] ],
    [ "IMixedRealityPrimaryPointerSelector.cs", "_i_mixed_reality_primary_pointer_selector_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityPrimaryPointerSelector", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_primary_pointer_selector.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_primary_pointer_selector" ]
    ] ],
    [ "IMixedRealityRaycastProvider.cs", "_i_mixed_reality_raycast_provider_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityRaycastProvider", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_raycast_provider.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_raycast_provider" ]
    ] ],
    [ "IMixedRealityTeleportPointer.cs", "_i_mixed_reality_teleport_pointer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityTeleportPointer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_teleport_pointer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_teleport_pointer" ]
    ] ],
    [ "IMixedRealityTouchPointer.cs", "_i_mixed_reality_touch_pointer_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchPointer", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_touch_pointer.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_touch_pointer" ]
    ] ],
    [ "IPointerPreferences.cs", "_i_pointer_preferences_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IPointerPreferences", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_pointer_preferences.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_pointer_preferences" ]
    ] ],
    [ "IPointerResult.cs", "_i_pointer_result_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Input.IPointerResult", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_pointer_result.html", "interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_pointer_result" ]
    ] ]
];