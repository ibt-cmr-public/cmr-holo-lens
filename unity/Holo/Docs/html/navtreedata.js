/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Essential Vision", "index.html", [
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", "functions_prop" ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_active_power_scheme_info_8cs.html",
"_clipping_sphere_inspector_8cs.html",
"_editor_assembly_reload_manager_8cs.html",
"_i_lost_tracking_service_8cs.html",
"_layout_anchor_8cs.html#a582a0f5a0bd8598ce5b85799a1e8c193a9146bfc669fddc88db2c4d89297d0e9a",
"_near_interaction_touchable_inspector_8cs.html",
"_scene_transition_service_8cs.html",
"_tool_tip_attach_point_8cs.html#a8ec41da48d0ef7afb590eab9da9d1f14a656ff122ca026980fc99acd8b5593ea3",
"class_color_map.html#a98676fbfcc4198e2278de259c39b0bd4",
"class_level_control_1_1_level_player_state_data.html#ad824fb7cccf7910f1638710500fac0f3",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_boundary_event_data.html#aa56a24c8b93bdb296e72c3da4d90a3b8",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_build_1_1_editor_1_1_build_info.html#aa63365c3a1a1182d03cdfc05764bcf84",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_core_services.html#a392eccfea246e5c1b35a3db1b0eef75e",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_clipping_plane_editor.html#a3343722303e998d09f56e8d2317f5107",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_shader_g_u_i.html#a231346116467df6e1778b0f3c37cec35",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_standard_shader_g_u_i.html#a8527e8a577cbb8eb3979b4d856427aba",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_editor_1_1_mixed_reality_standard_shader_g_u_i_1_1_styles.html#ac57df2fd46dcda32f9c684e1af79a2cd",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_enumerable_extensions.html",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_joystick_1_1_joystick_controller.html#a199f8b86c918ec0595dd16ee63514168",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_u_i_1_1_hand_coach_1_1_move_to_target.html#a9332b50b69e9d72ddc9fbf946a291a3c",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_experimental_1_1_u_i_1_1_non_native_keyboard.html#aeff3e6dbd3fc30f42b13b6034e0d2fc4",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_extensions_1_1_scene_transitions_1_1_camera_fader_quad.html#a4735e019912bc112a94ab1997e2353dd",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_base_controller.html#a0d31139d63b8f57064a62f8ace9fefc6",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_base_eye_focus_handler.html#aa7369e0f660a75ee7cf8a702a37822e9",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_controller_mapping_library.html#ad181c11448c9f593cf60a532a297f506",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_editor_1_1_input_mapping_axis_utility.html#a6f4f2dff56144b1f662fed60c579d190",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_g_g_v_pointer.html#a76d47f1993444c3304653981c97570cb",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_action_handler.html",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_input_simulation_service.html#a4f85868ba7d586f33f4da0c7d6f0d301",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_controller_visualization_profile.html#a53870dedd84432960e6a75d9397b49c5",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_input_simulation_profile.html#ad9d3f6adcfee1faf60ba3e2d81494332",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_interaction_mapping.html#ab7f9e4eda399705e5fcc536c8a441650",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_pointer_utils.html#a21f5d8331cac0f2979fdab683988bf65",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_speech_event_data.html#ab44dfde242be303eb8f87811b5d17420",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_windows_mixed_reality_controller_visualizer.html",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_m_s_build_1_1_reference_item_info.html#a12fae052908cd9aa39f3b91efdd6ac46",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_mixed_reality_service_registry.html#a1e0c278df0454ce2c0f8177931e94f43",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_chaperone_setup.html#ae9bd35e8248fa8eecc77244f1c0847c2",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_overlay.html#a62ca507fdd8faebae96939b26655a863",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_c_v_r_system.html#a730973221f82cc57c859463fcf63e365",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_open_v_r.html#a5dc885a1bf62fa86beacbba26db1cb4c",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_open_v_r.html#ad7eecfeedebbe610bf406a4f0daebe76",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_distorter_gravity.html#a8292b79bcbdda507fcaee0a117a1ec0e",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_scene_system_1_1_mixed_reality_scene_system.html",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_spatial_awareness_1_1_base_spatial_observer.html#ad3d57e68e823ae4efc1c8774f6138198",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_teleport_1_1_parabolic_teleport_pointer.html",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html#a0bcbdd052e4d0d12f6c4669fcfc6ba3f",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounding_box.html#a19c4b66761f3b572c4a87e7e948f741e",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_bounds_control_1_1_bounds_control.html#ae4e9714cc379a4ed779c676a17f54549",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_button_icon_set.html#a39a95dc52ee538e29e371715452b9c7f",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_follow_me_toggle.html#ae1a95fab652ceada5a698ffff014f101",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_interactable.html#a837b3fc5027d2234ad3ff741acfe539e",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_interactable_event.html#ace73f17b6610febf6397311c7590280a",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_interactable_receiver_list_inspector.html#a2803ec39a66520cb27b83841f37dbbd9",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_loader_controller.html#ad366bf9620c69088f1fc7647c6255ea7",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_pinch_slider.html#aad72cda8317ee784ed27a4f44c2ef957",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_rotation_axis_constraint.html#a285045ca86d2461179a7d20ee3f021af",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_theme_ease_settings.html#a1ce80f779adf53cf064b6ff15386bfa7ab5a2f8abee775e5f5f4c30e031739365",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_u_i_1_1_transform_constraint.html#ac1ab237a3442c4fbad0ee51777e71e55",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_base_object_collection.html#a5f69a60fa1e779dab7771390ed6e4554",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_assembly_definition.html#a2a2bf9e41958e2a806f6bcbee8565fde",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_inspector_u_i_utility.html#a126abdcdfe3447fdf7ca117ff9b62e15",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_mixed_reality_project_configurator.html#a08bd3b8ef39c84e221d5ae71d8250758afb1cda510ce9a36b7d51546b340fd64c",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_tool_tip_connector_inspector.html#ac855cd197b17d32dd05b3844aad3d360",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_gltf_1_1_schema_1_1_gltf_buffer_view.html#adf127710663aad033f5606b32c07294a",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_gltf_1_1_schema_1_1_gltf_texture_info.html#af6601ffece29dbaf81553cc3e7edc8ef",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_mesh_smoother.html#ac4ef2cbdcdedf21cd20d5f5b8874425c",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_rest.html#a0d461b583a85a265de442d5fa6fc363b",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_solvers_1_1_momentum.html#aec591263b4f2cde28b4f7a3a5d1796fa",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_solvers_1_1_tap_to_place.html#a23d166afa0db245232e2cb3517966161",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_windows_1_1_input_1_1_windows_dictation_input_provider.html#abfb478bcd0ce66bc6e0857548cb61edd",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_windows_device_portal_1_1_ip_config_info.html",
"class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_x_r_s_d_k_1_1_input_1_1_generic_x_r_s_d_k_controller.html",
"class_model_with_plate.html#a7f5c7dea9ae0bece0489faad295e2fe4a6adf97f83acf6453d4a6a4b1070f3754",
"dir_6ee986628135b094c390059b2545d1d3.html",
"functions_vars_s.html",
"interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_i_mixed_reality_data_provider_access.html",
"interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_cursor.html#ab8d062f5e859b4a3f8d811c7efa60bd5",
"interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_input_system.html#a1a957dfd715faacf6cb4315fcad21ca3",
"interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_i_mixed_reality_source_state_handler.html#acbd6989373a882222edff8674577c24d",
"interface_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_teleport_1_1_i_mixed_reality_teleport_handler.html#a77a916e8487d9170356521105cba7023",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_extensions_1_1_scene_transitions.html#ac28625c6f605cf985493266c1dbe7627a71ff71526d15db86eb50fcac245d183b",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers.html#a4465955d42c044bd0685fd0fbcf8fd23acf837c5650e4aaf70adc1934c574836e",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers.html#a7107ab706f1ec6924f79d0752dddddcbac9a0a95ece9629e2f771c5f0b4978c48",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers.html#aa87674d1b9e399c4b0f3e2630f9c7628acfbde305aae668f6080ad7e6dabdd03e",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers.html#ae02c6106bcce04ec6dab7bd1b2b6ff24a8fbf28156a4274377558edb7be9ce3e5",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities.html#a436955ec7cae41c0ee2440bcc929449ba04a83927cfa1af6ae14f94e90aab9ebb",
"namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_gltf_1_1_schema.html#a7e7debf2cc8175e409a6833530a67ac2",
"openvr__api_8cs.html#a5e2d92940d0808804d339eb4c6443ca6a563646a5f4d41d241147974b4d29cc10",
"openvr__api_8cs.html#a96c264d538def403f5bf25d8abc53400ac5c74fde889a7ff675d84f6a4b725b0d",
"openvr__api_8cs.html#ac7cda30a13f436a23e5ed10a4c90f2c6a505a83f220c02df2f85c3810cd9ceb38",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_boundary_1_1_edge.html#aac13c691220bfdb08a8c89be09990d55",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_input_1_1_mixed_reality_controller_visualization_setting.html#a4b052214c344e0bcc3c2f41fe1f620ac",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_compositor___frame_timing.html",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_hmd_vector2__t.html",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_input_origin_info__t.html#a746741da00aaffa5dc8ab4d9e67638a4",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_tracked_device_pose__t.html#a3653b05812ba676d712f7ab6ff99e9c0",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_open_v_r_1_1_headers_1_1_v_r_event___performance_test__t.html#a94a0f0dd81811e09179ffb3426ff4f5d",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_physics_1_1_ray_step.html#a0e9dc1ba3a74c673c9028940c1765dab",
"struct_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_utilities_1_1_editor_1_1_inspector_property_setting.html#af2d67e5c28e2bfccf4be656779caeedc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';