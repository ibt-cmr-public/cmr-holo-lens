var namespace_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests =
[
    [ "PlayModeTestUtilities", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_play_mode_test_utilities.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_play_mode_test_utilities" ],
    [ "TestController", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_controller" ],
    [ "TestHand", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_hand.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_hand" ],
    [ "TestMotionController", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_motion_controller" ],
    [ "TestUtilities", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_tests_1_1_test_utilities" ]
];