var dir_12155514c87618e129174ecc88ecad18 =
[
    [ "BasicSharingStatus.cs", "_basic_sharing_status_8cs.html", [
      [ "BasicSharingStatus", "class_basic_sharing_status.html", null ]
    ] ],
    [ "ButtonListScript.cs", "_button_list_script_8cs.html", [
      [ "ButtonListScript", "class_button_list_script.html", "class_button_list_script" ]
    ] ],
    [ "JoinSelectedSessionButton.cs", "_join_selected_session_button_8cs.html", [
      [ "JoinSelectedSessionButton", "class_join_selected_session_button.html", "class_join_selected_session_button" ]
    ] ],
    [ "OfflineModeButton.cs", "_offline_mode_button_8cs.html", [
      [ "OfflineModeButton", "class_offline_mode_button.html", "class_offline_mode_button" ]
    ] ],
    [ "PositionDebugButton.cs", "_position_debug_button_8cs.html", [
      [ "PositionDebugButton", "class_position_debug_button.html", "class_position_debug_button" ]
    ] ],
    [ "ResetAnchorButton.cs", "_reset_anchor_button_8cs.html", [
      [ "ResetAnchorButton", "class_reset_anchor_button.html", "class_reset_anchor_button" ]
    ] ],
    [ "ScrollableListPopulator.cs", "_scrollable_list_populator_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_examples_1_1_demos_1_1_scrollable_list_populator.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_examples_1_1_demos_1_1_scrollable_list_populator" ]
    ] ],
    [ "ScrollablePagination.cs", "_scrollable_pagination_8cs.html", [
      [ "Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_examples_1_1_demos_1_1_scrollable_pagination.html", "class_microsoft_1_1_mixed_reality_1_1_toolkit_1_1_examples_1_1_demos_1_1_scrollable_pagination" ]
    ] ],
    [ "ScrollingSessionListUIController.cs", "_scrolling_session_list_u_i_controller_8cs.html", [
      [ "ScrollingSessionListUIController", "class_scrolling_session_list_u_i_controller.html", "class_scrolling_session_list_u_i_controller" ]
    ] ],
    [ "SessionListButton.cs", "_session_list_button_8cs.html", [
      [ "SessionListButton", "class_session_list_button.html", "class_session_list_button" ]
    ] ],
    [ "StartSessionButton.cs", "_start_session_button_8cs.html", [
      [ "StartSessionButton", "class_start_session_button.html", "class_start_session_button" ]
    ] ],
    [ "ToggleDebugWindow.cs", "_toggle_debug_window_8cs.html", [
      [ "ToggleDebugWindow", "class_toggle_debug_window.html", "class_toggle_debug_window" ]
    ] ],
    [ "ToggleSharingUIButton.cs", "_toggle_sharing_u_i_button_8cs.html", [
      [ "ToggleSharingUIButton", "class_toggle_sharing_u_i_button.html", "class_toggle_sharing_u_i_button" ]
    ] ],
    [ "UIController.cs", "_u_i_controller_8cs.html", [
      [ "Assets.SharedExperience.Scripts.UI.UIController", "class_assets_1_1_shared_experience_1_1_scripts_1_1_u_i_1_1_u_i_controller.html", "class_assets_1_1_shared_experience_1_1_scripts_1_1_u_i_1_1_u_i_controller" ]
    ] ],
    [ "WireupDebugPanel.cs", "_wireup_debug_panel_8cs.html", [
      [ "WireupDebugPanel", "class_wireup_debug_panel.html", "class_wireup_debug_panel" ]
    ] ]
];