var class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter =
[
    [ "ConfigureAsServer", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html#ae98d00f40c69b7da8d4c86a87aaab8f5", null ],
    [ "OnDataReady", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html#aaca000c8e12b32e21552692ea77f4569", null ],
    [ "RequestAndGetData", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html#aca0a005ba95d4dc9ad81dab3850340fc", null ],
    [ "SetData", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html#a9992e18cc567b81b731a4aa0d55ab314", null ],
    [ "SendConnectionPort", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html#a13a503d89aa059516b65284299d551fe", null ],
    [ "dataReadyEvent", "class_holo_toolkit_1_1_examples_1_1_sharing_with_u_n_e_t_1_1_generic_network_transmitter.html#aaffe1ff3c71204e2b2526fe471faf478", null ]
];