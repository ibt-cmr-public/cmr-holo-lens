﻿using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.UI.BoundsControl;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.Experimental.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class ModelWithPlate : MonoBehaviour, IClickHandler
{
    /* Public fields that should be set in Unity Editor */
    public GameObject SliderAnimationSpeed;
    public GameObject ButtonsModel;
    public GameObject ButtonsModelPreview;
    public GameObject ButtonsModelSetting;
    public GameObject LayerSubmenu;
    public GameObject PlateAnimated;
    public GameObject HomeMenuConfirmation;

    public Material DefaultModelMaterial;
    public Material DefaultModelTransparentMaterial;
    /** The Material used to display flow or fibre asset bundle layers as points with vectors.
     * The colors and vecotr-length are determined by the colors resp. scalar specified in the vtk file of the pre-rpcesssing step.
     * Normally set to DataFlowMat.*/
    public Material DataVisualizationMaterial;
    /** The Material used to display turbulence asset bundle layers as small cube-clouds.
     * The colors are determind by the colors specified in the vtk file of the pre-processing step.
     * Normally set to DataTurbulenceMat.*/
    public Material DataTurbulenceMaterial;
    /** The Material used for multicolored asset bundle layers based on a one scalar value per point/vertecy. 
     * Normally set to DataDisplacementMat.*/
    public Material DataDisplacementMaterial;
    public Material DataDisplacementTransparentMaterial;
    /** The Material used for single colored asset bundle layers. 
     * Normally set to DataColorMat.
     * The color is set in the ModelConfig file in the pre-processing step.*/
    public Material DataColorMaterial;
    public Material DataColorTransparentMaterial;

    public Material DefaultVolumetricMaterial;

    public Transform InstanceParent;
    public PressableButtonHoloLens2 ButtonTogglePlay;
    public PressableButtonHoloLens2 ButtonLayerSubmenu;
    public PressableButtonHoloLens2 ButtonScrollUp;
    public PressableButtonHoloLens2 ButtonScrollDown;
    public PressableButtonHoloLens2 ButtonTransform;
    public PressableButtonHoloLens2 ButtonTranslate;
    public PressableButtonHoloLens2 ButtonRotate;
    public PressableButtonHoloLens2 ButtonScale;
    public PressableButtonHoloLens2 ButtonAnimationSubmenu;
    public PressableButtonHoloLens2 ButtonTransparency;
    public GameObject PlateButtonCollection;
    public GameObject ButtonLayerTemplate;
    public Texture2D ButtonIconPlay;
    public Texture2D ButtonIconPause;
    // Drop here "Prefabs/ModelWithPlateRotationRig"
    public GameObject RotationBoxRigTemplate;
    public GameObject AddButtonsCollection;

    /** Reference to the Color Map (Script) in the scene.
     * The Colormap is normally attached to the same Gameobject as this script.*/
    public ColorMenue ColorMap;

    private float SliderSpeedFactor = 5.0f;

    private int MaxButtonLayerNumber = 4; // buttonLayerHeight * MaxButtonLayerNumber = 8 for nice layout
    private int ScrollCount = 0;

    public enum TransformationState
    {
        None,
        Translate,
        Rotate,
        Scale
    }
    private TransformationState transformationState = TransformationState.None;
    private bool TranslationIsActive = true;
    private bool RotationIsAcitve = true;
    private bool ScalingIsAcitve = true;

    public GameObject ModelClipPlane;

    private ModelClippingPlaneControl ModelClipPlaneCtrl;

    public bool InstanceLoaded { get; private set; } = false;
    /* All the variables below are non-null
     * only when instanceLoaded,
     * that is only after LoadInstance call (and before UnloadInstance). */
    private AssetBundleLoader instanceBundle;
    private LayersLoaded layersLoaded;
    private Dictionary<ModelLayer, PressableButtonHoloLens2> layersButtons;
    private GameObject instanceTransformation;
    private bool instanceIsPreview = false;
    private DirectionalIndicator directionalIndicator;

    // Created only when instance != null, as it initializes bbox in Start and assumes it's not empty
    private GameObject rotationBoxRig;

    /* Currently loaded bundle name, null if none. */
    public string InstanceName
    {
        get
        {
            return instanceBundle != null ? instanceBundle.Name : null;
        }
    }

    public Vector3 ModelPosition
    {
        get
        {
            return rotationBoxRig != null ? rotationBoxRig.transform.localPosition : Vector3.one;
        }
        set
        {
            if (rotationBoxRig != null)
            {
                rotationBoxRig.transform.localPosition = value;
            }
            // TODO: otherwise ignore, we do not synchronize position for unloaded models now
        }
    }

    public Quaternion ModelRotation
    {
        get {
            return rotationBoxRig != null ? rotationBoxRig.transform.localRotation : Quaternion.identity;
        }
        set {
            if (rotationBoxRig != null) {
                rotationBoxRig.transform.localRotation = value;
            }
            // TODO: otherwise ignore, we do not synchronize rotation for unloaded models now
        }
    }

    public Vector3 ModelScale
    {
        get
        {
            return rotationBoxRig != null ? rotationBoxRig.transform.localScale : Vector3.one;
        }
        set
        {
            if (rotationBoxRig != null)
            {
                rotationBoxRig.transform.localScale = value;
            }
            // TODO: otherwise ignore, we do not synchronize scale for unloaded models now
        }
    }

    /* Currently visible layers, expressed as a bitmask. */
    public uint InstanceLayers
    {
        get
        {
            uint result = 0;
            if (layersLoaded != null) {
                foreach (ModelLayer layer in layersLoaded.Keys) {
                    result |= ((uint)1 << layer.LayerIndex);
                }
            }
            return result;
        }
        set
        {
            uint currentLayers = InstanceLayers;
            foreach (ModelLayer layer in instanceBundle.Layers) {
                uint layerMask = (uint)1 << layer.LayerIndex;
                if ((layerMask & value) != 0 && (layerMask & currentLayers) == 0) {
                    LoadLayer(layer);
                } else
                if ((layerMask & value) == 0 && (layerMask & currentLayers) != 0) {
                    UnloadLayer(layer);
                }
            }
        }
    }

    public void Start()
    {

        ModelClipPlaneCtrl = ModelClipPlane.GetComponentInChildren<ModelClippingPlaneControl>();
        directionalIndicator = gameObject.GetComponentInChildren<DirectionalIndicator>();
        // Turn off the clipping plane on start
        DefaultModelMaterial.DisableKeyword("CLIPPING_ON");
        DefaultModelTransparentMaterial.DisableKeyword("CLIPPING_ON");
        DataVisualizationMaterial.DisableKeyword("CLIPPING_ON");
        DataTurbulenceMaterial.DisableKeyword("CLIPPING_ON");
        DataDisplacementMaterial.DisableKeyword("CLIPPING_ON");
        DataDisplacementTransparentMaterial.DisableKeyword("CLIPPING_ON");
        DataColorMaterial.DisableKeyword("CLIPPING_ON");
        DataColorTransparentMaterial.DisableKeyword("CLIPPING_ON");

        LayerSubmenuState = false;
        AnimationSpeedSubmenu = false;

        RefreshUserInterface();
        InitializeAddButtons();

        // Activate platform movement
        ChangeTransformationState2(TransformationState.Translate);

        SliderAnimationSpeed.GetComponent<PinchSlider>().OnValueUpdated.AddListener(
            delegate
            {
                AnimationSpeed = SliderAnimationSpeed.GetComponent<PinchSlider>().SliderValue * SliderSpeedFactor;
                Debug.Log(SliderAnimationSpeed.GetComponent<PinchSlider>().SliderValue.ToString());
                SliderAnimationSpeed.transform.Find("ThumbRoot/SpeedValue").GetComponent<TextMeshPro>().text = Math.Round(AnimationSpeed, 2).ToString();
            }
        );

        
    }

    /* Number of "add" buttons we have in the scene. */
    private const int addButtonsCount = 15;

    /* Find the GameObject of some "AddXxx" button. */
    private GameObject FindAddButton(int i)
    {
        // TODO: Search for the real name
        return AddButtonsCollection.transform.Find("Add" + i.ToString()).gameObject;
    }

    /** Initialize "AddXxx" buttons captions and existence for asset bunbdles. */
    private void InitializeAddButtons()
    {
        if (ModelsCollection.Singleton == null)
        {
            Debug.LogError("ModelsCollection script must be executed before ModelWithPlate. Fix Unity \"Script Execution Order\".");
            return;
        }

        int activeButtonsCount = Mathf.Min(addButtonsCount, ModelsCollection.Singleton.BundlesCount);

        // set add buttons captions and existence, for the buttons that correspond to some bundles
        for (int i = 0; i < activeButtonsCount; i++)
        {
            GameObject button = FindAddButton(i);
            button.SetActive(true);

            string modelName = ModelsCollection.Singleton.BundleCaption(i);
            button.transform.Find("IconAndText/Text").GetComponent<TextMeshPro>().text = modelName;

            Texture2D icon = ModelsCollection.Singleton.BundleIcon(i);
            if (icon != null) {
                button.transform.Find("IconAndText/UIButtonSquareIcon").GetComponent<MeshRenderer>().material.SetTexture("_MainTex", icon);
                button.transform.Find("IconAndText/UIButtonSquareIcon").GetComponent<MeshRenderer>().enabled = true;
            }
        }

        // hide the rest of the buttons, when there are less models than buttons
        for (int i = activeButtonsCount; i < addButtonsCount; i++)
        {
            GameObject button = FindAddButton(i);
            button.SetActive(false);
        }
    }

    /** Handle a click on some button inside. Called by ButtonsClickReceiver. */
    public void Click(GameObject clickObject)
    {
        if (SharingSceneData.Singleton.isClient && !SharingSceneData.Singleton.isServer) 
        {
              return;
        }
        Debug.Log("ModelWithPlate Click: " + clickObject.name);

        switch (clickObject.name)
        {
            case "TogglePlay": ClickTogglePlay(); break;
            case "Rewind": ClickRewind(); break;
            case "Remove": ClickRemove(); break;
            case "Speed": ClickSpeed(); break;
            case "ConfirmPreview": ClickConfirmPreview(); break;
            case "CancelPreview": ClickCancelPreview(); break;
            case "ButtonPlateHome": ClickHome(); break;
            case "ButtonLayers": ClickToggleLayersState(clickObject.GetComponent<PressableButtonHoloLens2>()); break;
            case "ButtonScrollUp": ClickScrollUp();break;
            case "ButtonScrollDown": ClickScrollDown();break;
            case "ButtonTransform": ClickTransform(); break;
            case "ButtonTranslate": ChangeTransformationState2(TransformationState.Translate); break;
            case "ButtonRotate": ChangeTransformationState2(TransformationState.Rotate); break;
            case "ButtonScale": ChangeTransformationState2(TransformationState.Scale); break;
            case "ButtonClipping": ClickClipping(); break;
            case "ButtonAnimationSpeed": ClickAnimationSpeed(); break;
            case "ButtonTransparency": ClickTransparency(); break;
            case "ButtonPlateRotationLeft": ClickPlateRotation(true); break;
            case "ButtonPlateRotationRight": ClickPlateRotation(false); break;

            /*Activate or deactivate layer*/
            default:
                {
                    ModelLayer layer = ButtonOfLayer(clickObject);
                    if (layer != null)
                    {
                        ClickChangeLayerState(layer);
                    } else
                    {
                        const string addPrefix = "Add";
                        int addInstanceIndex;
                        if (clickObject.name.StartsWith(addPrefix) &&
                            int.TryParse(clickObject.name.Substring(addPrefix.Length), out addInstanceIndex))
                        {
                            ClickAdd(ModelsCollection.Singleton.BundleName(addInstanceIndex));
                            CloseSubmenus();
                        }
                    }
                    break;
                }
        }
    }

    private bool StoreManipulatableModelActiveState, StoreHandDraggableClipPlaneActiveState;
    public void FocusEnter(GameObject focusObject)
    {
        if (focusObject.name == "Slider")
        {
            StoreManipulatableModelActiveState = GetComponent<ObjectManipulator>().enabled;
            StoreHandDraggableClipPlaneActiveState = ModelClipPlane.GetComponent<ObjectManipulator>().enabled;
            GetComponent<ObjectManipulator>().enabled = false;
            ModelClipPlane.GetComponent<ObjectManipulator>().enabled = false;
        }
    }

    public void FocusExit(GameObject focusObject)
    {
        if (focusObject.name == "Slider")
        {
            GetComponent<ObjectManipulator>().enabled = StoreManipulatableModelActiveState;
            ModelClipPlane.GetComponent<ObjectManipulator>().enabled = StoreHandDraggableClipPlaneActiveState;
        }
    }
    public void ClickTogglePlay()
    {
        AnimationPlaying = !AnimationPlaying;
    }

    public void ClickRewind()
    {
        AnimationTime = 0f;
    }

    private void ClickRemove()
    {
        UnloadInstance();
        RefreshUserInterface();
    }

    private void ClickSpeed()
    {
        const float MaxSpeed = 5f;
        float newSpeed = Mathf.Min(MaxSpeed, AnimationSpeed * 2);
        AnimationSpeed = newSpeed;
        SliderAnimationSpeed.GetComponent<PinchSlider>().SliderValue = newSpeed / SliderSpeedFactor;
        Debug.Log(SliderAnimationSpeed.GetComponent<PinchSlider>().SliderValue.ToString());
    }

    private void ClickHome()
    {
        PlateAnimated.GetComponent<Animator>().enabled = false;
        gameObject.SetActive(false);
        HomeMenuConfirmation.SetActive(true);
    }

    private void ClickCancelPreview()
    {
        UnloadInstance();
        RefreshUserInterface();
    }
    public void ClickToggleLayersState(PressableButtonHoloLens2 button)
    {
        var layerState = LayerSubmenuState;
        CloseSubmenus();
        LayerSubmenuState = !layerState;
    }
    private void ClickScrollUp()
    {
        ScrollCount++;
        ChangeLayerButtonActivation();
        CheckScrollButtonsActivation();
    }
    private void ClickScrollDown()
    {
        ScrollCount--;
        ChangeLayerButtonActivation();
        CheckScrollButtonsActivation();
    }
    private void ChangeLayerButtonActivation()
    {
        int[] buttonindex = Enumerable.Range(0, layersButtons.Values.Count).ToArray();
        foreach (Tuple<PressableButtonHoloLens2, int>  button in layersButtons.Values.Zip(buttonindex , Tuple.Create)) 
        {

            if (ScrollCount * MaxButtonLayerNumber <= button.Item2 && (ScrollCount + 1) * MaxButtonLayerNumber > button.Item2)
            {
                button.Item1.gameObject.SetActive(true);
            }
            else 
            {
                button.Item1.gameObject.SetActive(false);
            }
            
        }
    }

    private void CheckScrollButtonsActivation() 
    {
        if (layersButtons != null)
        {
            if ((ScrollCount + 1) * MaxButtonLayerNumber >= layersButtons.Values.Count | layersButtons.Values.Count < 1)
            {
                ButtonScrollUp.gameObject.SetActive(false);
            }
            else
            {
                ButtonScrollUp.gameObject.SetActive(true);
            }

            if (ScrollCount == 0 | layersButtons.Values.Count < 1)
            {
                ButtonScrollDown.gameObject.SetActive(false);
            }
            else
            {
                ButtonScrollDown.gameObject.SetActive(true);
            }
        }
        else
        {
            ButtonScrollUp.gameObject.SetActive(false);
            ButtonScrollDown.gameObject.SetActive(false);
        }
        
    }
    private void ClickTransform()
    {
        var modelTransformState = ModelTransform;
        CloseSubmenus();
        ModelTransform = !modelTransformState;
    }
    private void ClickClipping()
    {
        var clipPlaneState = ModelClipPlaneCtrl.ClippingPlaneState;
        CloseSubmenus();
        ModelClipPlaneCtrl.ClippingPlaneState = clipPlaneState;
    }

    private void ClickAnimationSpeed()
    {
        var animationSubmenuState = AnimationSpeedSubmenu;
        CloseSubmenus();
        AnimationSpeedSubmenu = !animationSubmenuState;
    }

    private void ClickTransparency()
    {
        Transparent = !Transparent;
    }

    private void ClickPlateRotation(bool left)
    {
        CloseSubmenus();
        float currenstate = PlateButtonCollection.GetComponent<Transform>().rotation.z;
        if (left)
        { 
            currenstate -= 72; 
        }
        else
        {
            currenstate += 72;
        }   
        PlateButtonCollection.GetComponent<Transform>().Rotate(new Vector3 (0,0,currenstate));
        //PlateRotation = !PlateRotation;
    }

    /**Load new model or unload. NewInstanceBundleName must match asset bundle name, returned by AssetBundleLoader.Name. It can also be null or empty to unload a model. */
    public void SetInstance(string newInstanceBundleName)
    {
        if (!string.IsNullOrEmpty(newInstanceBundleName))
        {
            LoadInstance(newInstanceBundleName, false);
            LoadInitialLayers();
            ModelClipPlaneCtrl.ClippingPlaneState = ModelClippingPlaneControl.ClipPlaneState.Disabled;
        } else
        {
            UnloadInstance();
        }
        RefreshUserInterface(); // necessary after LoadInstance and UnloadInstance
    }

    private void ClickAdd(string newInstanceBundleName)
    {
        LoadInstance(newInstanceBundleName, true);
        LoadInitialLayers();
        RefreshUserInterface();
        ModelClipPlaneCtrl.ClippingPlaneState = ModelClippingPlaneControl.ClipPlaneState.Disabled;
    }

    private void ClickConfirmPreview()
    {
        //LoadInstance(instanceBundle.Name, false);
        //LoadInitialLayers();
        instanceIsPreview = false;
        RefreshUserInterface();
    }
    private void ClickChangeLayerState(ModelLayer layer)
    {
        if (!InstanceLoaded)
        {
            Debug.Log("No model loaded, cannot show layer.");
            return;
        }

        bool addLayer = !layersLoaded.ContainsKey(layer);
        if (!addLayer) {
            UnloadLayer(layer);
        } else {
            if (layer.DataType == DataType.Volumetric)
            {
                List<ModelLayer> layersToUnload = new List<ModelLayer>();
                foreach (KeyValuePair<ModelLayer, LayerLoaded> entry in layersLoaded)
                    layersToUnload.Add(entry.Key);

                foreach (ModelLayer l in layersToUnload)
                {
                    UnloadLayer(l);
                    HoloUtilities.SetButtonStateText(layersButtons[l], false);
                }
            }
            LoadLayer(layer);
        }
        HoloUtilities.SetButtonStateText(layersButtons[layer], addLayer);
    }

    public void RefreshUserInterface()
    {
        ButtonsModel.SetActive(InstanceLoaded && !instanceIsPreview);
        ButtonsModelPreview.SetActive(InstanceLoaded && instanceIsPreview);
        ButtonsModelSetting.SetActive(!InstanceLoaded && !instanceIsPreview);
        PlateVisible = (!InstanceLoaded) || instanceIsPreview;

        // update ButtonTogglePlay caption and icon
        bool playing = AnimationPlaying;
        string playOrPauseText = playing ? "PAUSE" : "PLAY";
        ButtonTogglePlay.GetComponent<ButtonConfigHelper>().MainLabelText = playOrPauseText;
        string playOrPauseIconName = playing ? ButtonIconPause.name : ButtonIconPlay.name;
        ButtonTogglePlay.GetComponent<ButtonConfigHelper>().SetQuadIconByName(playOrPauseIconName);

        CheckScrollButtonsActivation();
    }

    private void UnloadLayer(ModelLayer layer)
    {
        if (!layersLoaded.ContainsKey(layer)) {
            Debug.LogWarning("Cannot unload layer " + layer.Caption + ", it is not loaded yet");
            return;
        }
        foreach (var renderer in layersLoaded[layer].Instance.GetComponentsInChildren<Renderer>())
        {
            renderer.sharedMaterial = LayerMaterial(layer);
            ModelClipPlane.GetComponent<ClippingPlane>().RemoveRenderer(renderer);
        }

        Destroy(layersLoaded[layer].Instance);
        layersLoaded.Remove(layer);
        ColorMap.GetLoadedTypes();
    }

    /** Unload currently loaded model. May be safely called even when instance is already unloaded. 
    * LoadInstance() calls this automatically at the beginning. After calling this, remember to call RefreshUserInterface at some point. */
    private void UnloadInstance()
    {
        if (layersLoaded != null) {
            foreach (LayerLoaded l in layersLoaded.Values) {
                Destroy(l.Instance);
            }
            layersLoaded = null;
            ColorMap.LayersLoaded = null;
            ColorMap.GetLoadedTypes();
        }

        if (layersButtons != null) {
            foreach (PressableButtonHoloLens2 button in layersButtons.Values) {
                // remove from ButtonsClickReceiver.interactables
                button.GetComponent<Interactable>().IsEnabled = false;
                Destroy(button.gameObject);
            }
            layersButtons = null;
        }

        if (instanceTransformation != null)
        {
            Destroy(instanceTransformation);
            Destroy(rotationBoxRig);
            instanceBundle = null;
            instanceTransformation = null;
            rotationBoxRig = null;
            instanceIsPreview = false; // value does not matter, but for security better to set it to something well-defined
        }

        InstanceLoaded = false;
        directionalIndicator.DirectionalTarget = InstanceParent;
    }

    /* Resulting instance will be in box of max size instanceMaxSize,
     * with center in instanceMove above plate origin.
     * This should match the plate designed sizes.
     */
    const float instanceMaxSize = 1.0f;
    const float expandedPlateHeight = 0.4f;
    Vector3 instanceMove = new Vector3(0f, expandedPlateHeight + instanceMaxSize / 2f, 0f); // constant
    const float buttonLayerHeight = 2f; // buttonLayerHeight * MaxButtonLayerNumber = 8 for nice layout

    private bool plateVisible;
    private bool PlateVisible
    {
        get { return plateVisible; }
        set {
            if (plateVisible != value) {
                plateVisible = value;
                PlateAnimated.GetComponent<Animator>().enabled = true;
                PlateAnimated.GetComponent<Animator>().SetBool("expanded", value);
            }
        }
    }

    public void ChangeTransformationState2(TransformationState newState)
    {
        if (newState == TransformationState.Rotate && rotationBoxRig != null)
        {
            if (RotationIsAcitve)
            {
                rotationBoxRig.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.YAxis | AxisFlags.ZAxis;
                rotationBoxRig.GetComponent<BoundsControl>().RotationHandlesConfig.ShowHandleForY = false;
                InstanceParent.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.YAxis | AxisFlags.ZAxis;

                RotationIsAcitve = false;
            }
            else
            {
                rotationBoxRig.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.ZAxis;
                rotationBoxRig.GetComponent<BoundsControl>().RotationHandlesConfig.ShowHandleForY = true;
                InstanceParent.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.ZAxis;

                RotationIsAcitve = true;
            }
        }
        else if (newState == TransformationState.Scale && rotationBoxRig != null)
        {
            if (ScalingIsAcitve)
            {
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = true;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 1;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 1;
                rotationBoxRig.GetComponent<BoundsControl>().ScaleHandlesConfig.ShowScaleHandles = false;

                InstanceParent.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = true;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 1;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 1;

                ScalingIsAcitve = false;
            }
            else
            {
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = false;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 5;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 0.1f;
                rotationBoxRig.GetComponent<BoundsControl>().ScaleHandlesConfig.ShowScaleHandles = true;

                InstanceParent.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = false;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 5;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 0.1f;

                ScalingIsAcitve = true;
            }
        }
        else if (newState == TransformationState.Translate && rotationBoxRig != null)
        {
            if (TranslationIsActive)
            {
                GetComponent<ObjectManipulator>().enabled = false;
                TranslationIsActive = false;
            }
            else
            {
                GetComponent<ObjectManipulator>().enabled = true;
                TranslationIsActive = true;
            }
        }
        else if(rotationBoxRig != null)
        {
            if (!RotationIsAcitve)
            {
                rotationBoxRig.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.YAxis | AxisFlags.ZAxis;
                rotationBoxRig.GetComponent<BoundsControl>().RotationHandlesConfig.ShowHandleForY = false;
                InstanceParent.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.YAxis | AxisFlags.ZAxis;

                RotationIsAcitve = false;
            }
            else
            {
                rotationBoxRig.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.ZAxis;
                rotationBoxRig.GetComponent<BoundsControl>().RotationHandlesConfig.ShowHandleForY = true;
                InstanceParent.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.ZAxis;

                RotationIsAcitve = true;
            }

            if (!ScalingIsAcitve)
            {
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = true;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 1;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 1;
                rotationBoxRig.GetComponent<BoundsControl>().ScaleHandlesConfig.ShowScaleHandles = false;

                InstanceParent.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = true;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 1;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 1;

                ScalingIsAcitve = false;
            }
            else
            {
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = false;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 5;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 0.1f;
                rotationBoxRig.GetComponent<BoundsControl>().ScaleHandlesConfig.ShowScaleHandles = true;

                InstanceParent.GetComponent<MinMaxScaleConstraint>().RelativeToInitialState = false;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMaximum = 5;
                InstanceParent.GetComponent<MinMaxScaleConstraint>().ScaleMinimum = 0.1f;

                ScalingIsAcitve = true;
            }

            if (!TranslationIsActive)
            {
                GetComponent<ObjectManipulator>().enabled = false;
                TranslationIsActive = false;
            }
            else
            {
                GetComponent<ObjectManipulator>().enabled = true;
                TranslationIsActive = true;
            }
        }

        HoloUtilities.SetButtonState(ButtonTranslate, TranslationIsActive);
        HoloUtilities.SetButtonState(ButtonRotate, RotationIsAcitve);
        HoloUtilities.SetButtonState(ButtonScale, ScalingIsAcitve);

        if (!RotationIsAcitve && !ScalingIsAcitve && rotationBoxRig != null)
        {
            rotationBoxRig.GetComponent<BoundsControl>().Active = false;
        }
        else if (rotationBoxRig != null)
        {
            rotationBoxRig.GetComponent<BoundsControl>().Active = true;
        }
    }    

    public void ChangeTransformationState(TransformationState newState)
    {
        bool rotationBoxRigActiveOld = transformationState == TransformationState.Rotate;
        bool scaleBoxRigActiveOld = transformationState == TransformationState.Scale;

        if (newState == transformationState) {
            // clicking again on the same sidebar button just toggles it off
            newState = TransformationState.None;
        }
        transformationState = newState;

        HoloUtilities.SetButtonState(ButtonTranslate, newState == TransformationState.Translate);
        HoloUtilities.SetButtonState(ButtonRotate, newState == TransformationState.Rotate);
        HoloUtilities.SetButtonState(ButtonScale, newState == TransformationState.Scale);

        // if transform the model: disable clipping plane manipulator
        if (newState != TransformationState.None && ModelClipPlaneCtrl.ClippingPlaneState != ModelClippingPlaneControl.ClipPlaneState.Disabled)
            ModelClipPlaneCtrl.ClippingPlaneState = ModelClippingPlaneControl.ClipPlaneState.Active;

        if (newState == TransformationState.Translate)
        {
            GetComponent<ObjectManipulator>().enabled = !GetComponent<ObjectManipulator>().enabled;
        }

        bool rotationBoxRigActiveNew = newState == TransformationState.Rotate;
        if (rotationBoxRigActiveOld != rotationBoxRigActiveNew && rotationBoxRig != null)
        {
            if (rotationBoxRigActiveNew) {
                rotationBoxRig.GetComponent<BoundsControl>().Active = true;
                rotationBoxRig.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.ZAxis;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().enabled = false; //if fales, rotation and scaling can be done together
            } 
        }

        /* As with rotationBoxRig, note that you cannot toggle enabled below.
         * For now, GetComponent<BoundingBoxRig>() is just enabled all the time. */
        bool scaleBoxRigActiveNew = newState == TransformationState.Scale;
        if (scaleBoxRigActiveOld != scaleBoxRigActiveNew)
        {
            if (scaleBoxRigActiveNew) {
                rotationBoxRig.GetComponent<BoundsControl>().Active = true;
                rotationBoxRig.GetComponent<RotationAxisConstraint>().ConstraintOnRotation = AxisFlags.XAxis | AxisFlags.ZAxis;
                rotationBoxRig.GetComponent<MinMaxScaleConstraint>().enabled = false;
            }
        }

        bool translateActiveNew = newState == TransformationState.Translate;
        if (!rotationBoxRigActiveNew && !scaleBoxRigActiveNew && !translateActiveNew && rotationBoxRig != null)
        {
            rotationBoxRig.GetComponent<BoundsControl>().Active = false;
        }
    }

    /** Does this button toggles some layer (returns null if not), and which one. */
    private ModelLayer ButtonOfLayer(GameObject buttonGameObject)
    {
        if (layersButtons != null) {
            foreach (var pair in layersButtons) {
                if (pair.Value.gameObject == buttonGameObject) {
                    return pair.Key;
                }
            }
        }
        return null;
    }

    /*! Load new model.
    
    newInstanceBundleName is a bundle name known to the ModelsCollection bundle.
    No layer is initially loaded -- you usually want to call
    LoadLayer immediately after this.
    After calling this, remember to call RefreshUserInterface at some point.*/
    private void LoadInstance(string newInstanceBundleName, bool newIsPreview)
    {
        if(instanceBundle != null)
        {
            Debug.Log("LoadInstance - currentInstanceName: " + instanceBundle.Name + ", newInstanceName: " + newInstanceBundleName);
            if(instanceIsPreview != newIsPreview && instanceBundle.Layers.All(l => l.DataType == DataType.Volumetric))
            {
                Debug.Log("LoadInstance - preview accepted!");
                instanceIsPreview = newIsPreview;
                return;
            }
        }            

        UnloadInstance();

        InstanceLoaded = true;
        instanceBundle = ModelsCollection.Singleton.BundleLoad(newInstanceBundleName);
        // Load Volumetric Data 
        if(instanceBundle.Layers.All(x => x.GetComponent<ModelLayer>().DataType == DataType.Volumetric))
        {
            instanceBundle.VolumetricMaterial = DefaultVolumetricMaterial;
            instanceBundle.LoadVolumetricData();
        }
        instanceIsPreview = newIsPreview;
        layersLoaded = new LayersLoaded();
        ColorMap.LayersLoaded = layersLoaded;

        rotationBoxRig = Instantiate<GameObject>(RotationBoxRigTemplate, InstanceParent.transform);

        instanceTransformation = new GameObject("InstanceTransformation");
        instanceTransformation.transform.parent = rotationBoxRig.transform;


       
        Vector3 boundsSize = Vector3.one;
        float scale = 1f;
        if (instanceBundle.Bounds.HasValue) { 
            Bounds b = instanceBundle.Bounds.Value;
            boundsSize = b.size;            
            float maxSize = Mathf.Max(new float[] { boundsSize.x, boundsSize.y, boundsSize.z });
            if (maxSize > Mathf.Epsilon) {
                scale = instanceMaxSize / maxSize;
            }
            instanceTransformation.transform.localScale = new Vector3(scale, scale, scale);
            instanceTransformation.transform.localPosition = -b.center * scale + instanceMove;

            //FIXME: this is a hack on instance not rotating properly if we move plate
            instanceTransformation.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);
        }

        // set proper BoxCollider bounds
        BoxCollider rotationBoxCollider = rotationBoxRig.GetComponent<BoxCollider>();
        rotationBoxCollider.center = instanceMove;
        rotationBoxCollider.size = boundsSize * scale;
        // Disable the component, to not prevent mouse clicking on buttons.
        // It will be taken into account to calculate bbox in BoundingBoxRig anyway,
        // since inside BoundingBox.GetColliderBoundsPoints it looks at all GetComponentsInChildren<Collider>() .

        // reset animation speed slider to value 1, animation time to 0 and set animation playing on true
        animationPlaying = true;
        animationTime = 0f;
        animationSpeed = 1f;
        SliderAnimationSpeed.GetComponent<PinchSlider>().SliderValue = animationSpeed / SliderSpeedFactor;

        // reset transparency to false
        Transparent = false;

        //set ScrollCount to 0
        ScrollCount = 0;

        // add buttons to toggle layers
        layersButtons = new Dictionary<ModelLayer, PressableButtonHoloLens2>();
        int buttonIndex = 0;
        foreach (ModelLayer layer in instanceBundle.Layers)
        {
            // add button to scene
            GameObject buttonGameObject = Instantiate<GameObject>(ButtonLayerTemplate, LayerSubmenu.transform);
            // buttonLayerHeight * MaxButtonLayerNumber = 8 for nice layout
            buttonGameObject.transform.localPosition =
                buttonGameObject.transform.localPosition + new Vector3(0f, 0f, buttonLayerHeight*MaxButtonLayerNumber - buttonLayerHeight *(buttonIndex%MaxButtonLayerNumber)) ;
            //if buttonindex is bigger than MaxButtonLayerNumber => deactivate Button
            if (buttonIndex >= MaxButtonLayerNumber)
            {
                buttonGameObject.SetActive(false);
            }
            buttonIndex++;
            // configure button text
            PressableButtonHoloLens2 button = buttonGameObject.GetComponent<PressableButtonHoloLens2>();
            if (button == null) {
                Debug.LogWarning("Missing component PressableButtonHoloLens2 in ButtonLayerTemplate");
                continue;
            }
            button.GetComponent<ButtonConfigHelper>().MainLabelText = layer.Caption;
            button.GetComponent<Interactable>().OnClick.AddListener(() => Click(buttonGameObject));
            
            // update layersButtons dictionary
            layersButtons[layer] = button;
        }
        // set directional indicator on rotationBoxRig
        //directionalIndicator.DirectionalTarget = instanceTransformation.transform;
        directionalIndicator.DirectionalTarget = rotationBoxRig.transform;

        // check activation of ButtonScrollUp and ButtonScrollDown
        CheckScrollButtonsActivation();

        // activate instance rotation and scaling
        ChangeTransformationState2(TransformationState.None);


        // reset object manipulation handler of InstanceParent
        InstanceParent.transform.localPosition = new Vector3(0f, 0f, 0f);
        InstanceParent.transform.rotation = Quaternion.Euler(0, 0, 0); ;
        InstanceParent.transform.localScale = new Vector3(1, 1, 1);
    }

    private void LoadInitialLayers()
    {
        if (!InstanceLoaded)
        {
            throw new Exception("Cannot call TodoLoadMeshLayer before LoadInstance");
        }

        Assert.IsTrue(instanceBundle != null);
        ModelLayer layer = instanceBundle.Layers.First<ModelLayer>(l => !l.Simulation & !l.Turbulence);
        LoadLayer(layer);
        HoloUtilities.SetButtonStateText(layersButtons[layer], true);
    }

    /** Instantiate new animated layer from currently loaded model.
     * If the layer is already instantiated, does nothing
     * (for now it makes a warning, but we can disable the warning if it's useful).
     *
     * This can only be used after LoadInstance (and before corresponding UnloadInstance).
     *
     * After calling this, remember to call RefreshUserInterface at some point.
     */
    private void LoadLayer(ModelLayer layer)
    {
        if (!InstanceLoaded)
        {
            throw new Exception("Cannot call LoadLayer before LoadInstance");
        }
        if (layersLoaded.ContainsKey(layer))
        {
            Debug.LogWarning("Layer already loaded");
            return;
        }

        var layerLoaded = new LayerLoaded();
        layerLoaded.Instance = layer.InstantiateGameObject(instanceTransformation.transform);

        layerLoaded.Animation = layerLoaded.Instance.GetComponent<BlendShapeAnimation>();
        if (layerLoaded.Animation != null)
        { 
            layerLoaded.Animation.InitializeBlendShapes();
            layerLoaded.Animation.Playing = AnimationPlaying;
            layerLoaded.Animation.CurrentTime = AnimationTime;
            layerLoaded.Animation.SpeedNormalized = AnimationSpeed;
        }

        // Assign material to all MeshRenderer and SkinnedMeshRenderer inside
        foreach (var renderer in layerLoaded.Instance.GetComponentsInChildren<Renderer>()) { 
            renderer.sharedMaterial = LayerMaterial(layer);
            ModelClipPlane.GetComponent<ClippingPlane>().AddRenderer(renderer);
        }

        {

            Debug.Log("MeshRenderer Loaded to the Clipping Plane");
        }

       // update layersLoaded dictionary
       layersLoaded[layer] = layerLoaded;

        // update colormap keys
        ColorMap.GetLoadedTypes();
    }

    /* Returns BlendShapeAnimation within any LayerLoaded with Animation component != null 
     * (that is, from any layer animated using out blend shape mechanism).
     * Returns null if no such layer exists (e.g. because there's no model loaded,
     * or it has no layers instantiated, or no layers instantiated with BlendShapeAnimation).
     */
    private BlendShapeAnimation AnyAnimatedLayer()
    {
        if (layersLoaded != null) { 
            foreach (LayerLoaded layerLoaded in layersLoaded.Values) {
                if (layerLoaded.Animation != null) {
                    return layerLoaded.Animation;
                }
            }
        }
        return null;
    }

    /* Is the animation currently playing.
     * When there are no instantiated animated layers 
     * (including when there is no instantiated model at all),
     * this always returns true and setting it is ignored.
     */
    private bool animationPlaying;
    public bool AnimationPlaying
    {
        get
        {
            //BlendShapeAnimation animatedLayer = AnyAnimatedLayer();
            //return animatedLayer != null ? animatedLayer.Playing : true;
            return animationPlaying;
        }
        set
        {
            animationPlaying = value;
            if (layersLoaded != null) {
                foreach (LayerLoaded l in layersLoaded.Values) {
                    if (l.Animation != null) { 
                        l.Animation.Playing = value;
                    }
                }
                if(!value)
                {
                    // save animationTime if pause
                    animationTime = layersLoaded.Values.First().Animation.CurrentTime;
                }
                RefreshUserInterface();
            } else {
                Debug.LogWarning("AnimationPlaying changed, but no animated layer loaded");
            }
        }
    }

    /* Current time within an animation.
     * When there are no instantiated animated layers 
     * (including when there is no instantiated model at all),
     * this always returns 0f and setting it is ignored.
     */
    private float animationTime;
    public float AnimationTime
    {
        get {
            BlendShapeAnimation animatedLayer = AnyAnimatedLayer();
            return animatedLayer != null ? animatedLayer.CurrentTime : animationTime;
            //return animatedLayer != null ? animatedLayer.CurrentTime : 0f;
        }
        set
        {
            animationTime = value;
            if (layersLoaded != null) {
                foreach (LayerLoaded l in layersLoaded.Values) {
                    if (l.Animation != null) { 
                        l.Animation.CurrentTime = value;
                    }
                }
                RefreshUserInterface();
            } else {
                Debug.LogWarning("AnimationTime changed, but no animated layer loaded");
            }
        }
    }

    private float animationSpeed = 1f;
    public float AnimationSpeed
    {
        get { return animationSpeed; }
        set {
            animationSpeed = value;

            if (layersLoaded != null) {
                foreach (LayerLoaded l in layersLoaded.Values) {
                    if (l.Animation != null) { 
                        l.Animation.SpeedNormalized = value;
                    }
                }
                RefreshUserInterface();
            }
        }
    }

    /* Based on layer properties, and current properties like Transparent,
     * determine proper material of the layer.
     */
    private Material LayerMaterial(ModelLayer layer)
    {
        Debug.Log("Getting LayerMaterial for layer: " + layer.name);
        if (layer.DataType == DataType.Volumetric)
        {
            return DefaultVolumetricMaterial;
        } else
        if (layer.Simulation)
        {
            return DataVisualizationMaterial;
        } else
        if (layer.Turbulence)
        {
            return DataTurbulenceMaterial;
        } else
        if (layer.Displacement)
        {

            if (Transparent)
            {
                return DataDisplacementTransparentMaterial;
            }
            else
            {
                return DataDisplacementMaterial;
            }
        } else
        if (layer.RGB)
        {

            if (Transparent)
            {
                return DataColorTransparentMaterial;
            }
            else
            {
                return DataColorMaterial;
            }
        } else
        if (layer.SpecialMaterial != null & layer.SpecialMaterial != "")
        {
            return Resources.Load<Material>("Specialmaterials/" + layer.SpecialMaterial);
        } else
        {
            if (Transparent)
            {
                return DefaultModelTransparentMaterial;
            }
            else
            {
                return DefaultModelMaterial;
            }
        }
    }

    private bool transparent;
    public bool Transparent {
        get { return transparent; }
        set
        {
            transparent = value;

            // Transparent value changed, so update materials
            if (layersLoaded != null) { 
                foreach (var layerPair in layersLoaded)
                {
                    if (!layerPair.Key.Simulation & !layerPair.Key.Turbulence) {
                        foreach (var renderer in layerPair.Value.Instance.GetComponentsInChildren<Renderer>()) { 
                            renderer.sharedMaterial = LayerMaterial(layerPair.Key);
                        }
                    }
                }
            }
            HoloUtilities.SetButtonState(ButtonTransparency, Transparent);
        }
    }

    private bool layerSubmenuState;
    public bool LayerSubmenuState {
        get { return layerSubmenuState; }
        set
        {
            layerSubmenuState = value;
            HoloUtilities.SetButtonState(ButtonLayerSubmenu, value);
            LayerSubmenu.SetActive(value);

        }

    }

    private bool animationSpeedSubmenu;
    public bool AnimationSpeedSubmenu {
        get { return animationSpeedSubmenu; }
        set
        {
            animationSpeedSubmenu = value;
            HoloUtilities.SetButtonState(ButtonAnimationSubmenu, value);
            ButtonAnimationSubmenu.gameObject.transform.parent.Find("AnimationSpeedSubMenu").gameObject.SetActive(value);

        }
    }

    /**private bool plateRotation = false;
    public bool PlateRotation{
        get { return plateRotation; }
        set
        {
            plateRotation = value;
            HoloUtilities.SetButtonState(ButtonPlateRotation, value);
            PlateButtonCollection.GetComponent<ObjectManipulator>().enabled = value;
        }
    }**/

    private void CloseSubmenus()
    {
        LayerSubmenuState =  false;
        ModelTransform = false;
        ModelClipPlaneCtrl.ClippingPlaneState = ModelClippingPlaneControl.ClipPlaneState.Disabled;
        AnimationSpeedSubmenu = false;
    }

    private bool modelTransform;
    public bool ModelTransform {
        get { return modelTransform; }
        set
        {
            modelTransform = value;
            HoloUtilities.SetButtonState(ButtonTransform, value);
            if (value == false)
            {
                ChangeTransformationState2(TransformationState.None);
            }
            ButtonTranslate.gameObject.SetActive(modelTransform);
            ButtonRotate.gameObject.SetActive(modelTransform);
            ButtonScale.gameObject.SetActive(modelTransform);
        }
    }
}
