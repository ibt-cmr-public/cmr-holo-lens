﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;
using TMPro;

public class ColorMenue : MonoBehaviour
{
    public GameObject colorPlate;
    public GameObject layerTypeKey;
    public GameObject leftArrow;
    public GameObject rightArrow;
    public GameObject ColorMenueObject;

    public GameObject minScaleValue;
    public GameObject maxScaleValue;
    public GameObject unitScaleValue;

    /* Configure colormap used by DataVisualizationMaterial and DataTurbulenceMaterial. */
    public Material DataVisualizationMaterial;
    public Material DataTurbulenceMaterial;
    public Material DataDisplacementMaterial;
    public Material DataDisplacementTransparentMaterial;
    public List<GameObject> ColorButtons;

    // must be set if there's a model currently loaded
    public LayersLoaded LayersLoaded;
    public List<string> loadedTypes = new List<string> {"Simulation", "Turbulence", "Displacement"};
    private int scrollingCounter = 0;

    // Map from button instance to the corresponding colormap name (texture name)
    private Dictionary<GameObject, string> colorMapButtons;

    private GameObject FindButton(string name)
    {
        foreach (GameObject buttonGameObject in ColorButtons)
        {
            if (buttonGameObject.name == name)
            {
                PressableButtonHoloLens2 button = buttonGameObject.GetComponent<PressableButtonHoloLens2>();
                if (button == null)
                {
                    throw new Exception("Colormap named " + name + " found, but it is not a button");
                }
                return buttonGameObject;
            }
        }
        throw new Exception("Colormap " + name + " not found");
    }

    void Start()
    {
        colorMapButtons = new Dictionary<GameObject, string>()
    {
        { FindButton("ButtonColorMapJet"), "jet" },
        { FindButton("ButtonColorMapViridis"), "viridis" },
        { FindButton("ButtonColorMapMagma"), "magma" },
        { FindButton("ButtonColorMapCoolwarm"), "coolwarm" }
    };

        // Load Player Settings for all loaded keys
        foreach (string key in loadedTypes)
        {
            string initialColorMap = PlayerPrefs.GetString("ColorMap" + key, "coolwarm");
            GameObject initialColorMapButton = ButtonFromColorMapName(initialColorMap);
            layerTypeKey.GetComponent<TextMeshPro>().text = key;
            ClickSetColorMap(initialColorMap, initialColorMapButton, key);
        }
        scrollingCounter = 0;
        ColorMenueObject.SetActive(false);
    }
          
    /* Reverse colorMapButtons map. */
    private GameObject ButtonFromColorMapName(string colorMapName)
    {
        foreach (var colorMapInfo in colorMapButtons)
        {
            if (colorMapInfo.Value == colorMapName)
            {
                return colorMapInfo.Key;
            }
        }
        throw new Exception("No colormap named " + colorMapName);
    }

    public void GetLoadedTypes()
    {
        loadedTypes = new List<string> { };
        if (LayersLoaded != null)
        {
            // check which simulaion types are loaded
            foreach (var layerPair in LayersLoaded)
            {
                if (layerPair.Key.Simulation & !loadedTypes.Contains("Simulation"))
                {
                    loadedTypes.Add("Simulation");
                }
                if (layerPair.Key.Turbulence & !loadedTypes.Contains("Turbulence"))
                {
                    loadedTypes.Add("Turbulence");
                }
                if (layerPair.Key.Displacement & !loadedTypes.Contains("Displacement"))
                {
                    loadedTypes.Add("Displacement");     
                }
            }
        }
        CheckColorMenue();
        Debug.Log(loadedTypes.Count);
    }

    public void Click(GameObject clickObject)
    {
        string colorMapName;
        if (colorMapButtons.TryGetValue(clickObject, out colorMapName))
        {
            ClickSetColorMap(colorMapName, clickObject, layerTypeKey.GetComponent<TextMeshPro>().text);
        }
    }

    private void ClickSetColorMap(string colorMapName, GameObject currentButton, string key)
    {
        MapName = colorMapName;
        foreach (GameObject button in colorMapButtons.Keys)
        {
            HoloUtilities.SetButtonStateText(button.GetComponent<PressableButtonHoloLens2>(), currentButton == button);
        }
        SetColorPlate(colorMapName);
        PlayerPrefs.SetString("ColorMap" + key, colorMapName);
        PlayerPrefs.Save();
    }

    private void UpdateColorMenue(string key)
    {
        string ColorMapName = PlayerPrefs.GetString("ColorMap" + key, "coolwarm");
        SetColorPlate(ColorMapName);
        SetColorScaleVariables(key);
        layerTypeKey.GetComponent<TextMeshPro>().text = key;
    }

    private void CheckColorMenue()
    {
        if (loadedTypes.Count == 0)
        {
            scrollingCounter = 0;
            ColorMenueObject.SetActive(false);
        }
        else if (loadedTypes.Count == 1)
        {
            scrollingCounter = 0;
            ColorMenueObject.SetActive(true);
            leftArrow.SetActive(false);
            rightArrow.SetActive(false);
            UpdateColorMenue(loadedTypes[scrollingCounter]);
        }
        else
        {
            scrollingCounter = loadedTypes.Count-1;
            ColorMenueObject.SetActive(true);
            leftArrow.SetActive(true);
            rightArrow.SetActive(true);
            UpdateColorMenue(loadedTypes[scrollingCounter]);
        }
    }

    public void ScrollLeft()
    {
        scrollingCounter -= 1;
        if (scrollingCounter < 0)
        {
            if (loadedTypes.Count > 0)
            {
                scrollingCounter = loadedTypes.Count - 1;
            }
            else
            {
                scrollingCounter = 0;
            }
            
        }
        UpdateColorMenue(loadedTypes[scrollingCounter]);

    }

    public void ScrollRight()
    {
        scrollingCounter += 1;
        if (scrollingCounter > loadedTypes.Count-1)
        {
            scrollingCounter = 0;
        }
        UpdateColorMenue(loadedTypes[scrollingCounter]);
    }

    public void SetColorPlate(string colorMapName)
    {
        Material colorMapMaterial = Resources.Load<Material>("Colormaterials/" + colorMapName);
        colorPlate.GetComponent<MeshRenderer>().material = colorMapMaterial;
    }

    // it only works if one layer per key type is activated at the same time! Always shows the oldest/first activate layer of a key type.
    private void SetColorScaleVariables(string key)
    {
        bool hasValues = false;
        if (LayersLoaded != null)
        {
            foreach (var layerPair in LayersLoaded)
            {
                if (layerPair.Key.Simulation & key == "Simulation" & layerPair.Key.ColorScaleVariables != null & layerPair.Key.ColorScaleVariables != "")
                {
                    SetVariables(layerPair.Key.ColorScaleVariables);
                    hasValues = true;
                    break;
                }
                if (layerPair.Key.Turbulence & key == "Turbulence" & layerPair.Key.ColorScaleVariables != null & layerPair.Key.ColorScaleVariables != "")
                {
                    SetVariables(layerPair.Key.ColorScaleVariables);
                    hasValues = true;
                    break;
                }
                if (layerPair.Key.Displacement & key == "Displacement" & layerPair.Key.ColorScaleVariables != null & layerPair.Key.ColorScaleVariables != "")
                {
                    SetVariables(layerPair.Key.ColorScaleVariables);
                    hasValues = true;
                    break;
                }
            }
        }
        if (!hasValues)
        {
            minScaleValue.GetComponent<TextMeshPro>().text = "";
            maxScaleValue.GetComponent<TextMeshPro>().text = "";
            unitScaleValue.GetComponent<TextMeshPro>().text = "";
        }
    }

    /* splits the string min-max-unit into the individual values and updates the Gameobjects of the colorscale with the text.*/
    private void SetVariables(string variables)
    {
       
        String[] strlist = variables.Split(new char[] { '-' }, 3);
        if (strlist.Length == 3)
        {
            minScaleValue.GetComponent<TextMeshPro>().text = strlist[0];
            maxScaleValue.GetComponent<TextMeshPro>().text = strlist[1];
            unitScaleValue.GetComponent<TextMeshPro>().text = strlist[2];
        }
        else
        {
            Debug.LogWarning("ColorScaleVariable was not set correctly (min-max-unit): " + variables);
            minScaleValue.GetComponent<TextMeshPro>().text = "";
            maxScaleValue.GetComponent<TextMeshPro>().text = "";
            unitScaleValue.GetComponent<TextMeshPro>().text = "";
        }
        
    }

    private string mapName;
    public string MapName
    {
        get
        {
            return mapName;
        }
        set
        {
            if (mapName != value)
            {
                mapName = value;
                Texture2D colorMapTexture = Resources.Load<Texture2D>("Colormaps/" + value);
                string key = layerTypeKey.GetComponent<TextMeshPro>().text;
                if (key == "Simulation")
                {
                    DataVisualizationMaterial.SetTexture("_ColorMap", colorMapTexture);
                }
                else if (key == "Turbulence")
                {
                    DataTurbulenceMaterial.SetTexture("_ColorMap", colorMapTexture);
                }
                else if (key == "Displacement")
                {
                    DataDisplacementMaterial.SetTexture("_ColorMap", colorMapTexture);
                    DataDisplacementTransparentMaterial.SetTexture("_ColorMap", colorMapTexture);
                }

                /* Change also current instances of materials, because
                    MaterialInstance.cs from Mrtk (required by ClippingPlane of Mrtk)
                    changes shared materials into non-shared. */
                if (LayersLoaded != null)
                {
                    foreach (var layerPair in LayersLoaded)
                    {
                        if (layerPair.Key.Simulation & key == "Simulation")
                        {
                            foreach (var renderer in layerPair.Value.Instance.GetComponentsInChildren<Renderer>())
                            {

                                renderer.material.SetTexture("_ColorMap", colorMapTexture);
                            }
                        }
                        if (layerPair.Key.Turbulence & key == "Turbulence")
                        {
                            foreach (var renderer in layerPair.Value.Instance.GetComponentsInChildren<Renderer>())
                            {
                                renderer.material.SetTexture("_ColorMap", colorMapTexture);
                            }
                        }
                        if (layerPair.Key.Displacement & key == "Displacement")
                        {
                            foreach (var renderer in layerPair.Value.Instance.GetComponentsInChildren<Renderer>())
                            {
                                renderer.material.SetTexture("_ColorMap", colorMapTexture);
                            }
                        }
                    }
                }
            }
        }
    }

    public void FocusEnter(GameObject focusEnterObject)
    {
        // does nothing, implemented only to satisfy IClickHandler interface
    }

    public void FocusExit(GameObject focusExitObject)
    {
        // does nothing, implemented only to satisfy IClickHandler interface
    }
    }
